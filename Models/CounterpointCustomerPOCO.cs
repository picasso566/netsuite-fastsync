﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace netsuite_fastsync
{
    //Comes from the NetSuite_VI_Customer view so all the field names are different
    public class CounterpointCustomerPOCO : HashBasePOCO
    {
        public IEnumerable<CounterpointCustomerAddressPOCO> Addresses { get; set; }

        public string CustomerId { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Category { get; set; }
        public string Comments { get; set; }
        public string Email { get; set; }
        public string AltEmail { get; set; }
        public string Phone { get; set; }
        public string AltPhone { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string Fax { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? _LastSalesActivity { get; set; }
        public DateTime? DriversLicenseDateOfBirth { get; set; }
        public string DriversLicenseEyeColor { get; set; }
        public string DriversLicenseHairColor { get; set; }
        public string DriversLicenseGender { get; set; }
        public string DriversLicenseHeightVarchar { get; set; }
        public int? DriversLicenseHeightMeasure { get; set; }
        public string DriversLicenseNo { get; set; }
        public string DriversLicenseRace { get; set; }
        public decimal? DriversLicenseWeight { get; set; }
        public string MARKETING_MAILOUTS_OPT_OUT { get; set; }
        public DateTime? MARKETING_MAILOUTS_OPT_OUT_DAT { get; set; }
        public string GsvVendNo { get; set; }
        public string TradeAppPhone { get; set; }
        public string GsvFreeShipping { get; set; }
        public decimal? GsvShippingLabelPrice { get; set; }
        public decimal? GsvMinimumProductBidPrice { get; set; }
        public string GsvSiteGUID { get; set; }
        public string GsvTradeAppFacilityName { get; set; }
        public string GsvHostSiteGUID { get; set; }
        public string NetSuiteInternalCustomerId { get; set; }
        public int VendorUpserted { get; set; }
    }
}
