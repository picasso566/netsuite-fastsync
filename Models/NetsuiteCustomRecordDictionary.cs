﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;

namespace netsuite_fastsync
{

    public class NetsuiteCustomRecordDictionaryList : Dictionary<string, NetsuiteCustomRecordDictionary>
    {
        public void AddToDictionary(string TypeName, IEnumerable<CustomRecord> ListResults)
        {
            this.Add(TypeName, new NetsuiteCustomRecordDictionary(TypeName, ListResults));
        }

        public void MergeList(NetsuiteCustomRecordDictionaryList ListToMerge)
        {
            ListToMerge.Keys.AsList().ForEach(k => this.Add(k, ListToMerge[k]));
        }

        public int TotalRecords
        {
            get
            {
                return this.Keys.AsList().Sum(x => this[x].TotalValues );
            }
        }
    }

    public class NetsuiteCustomRecordDictionary : Dictionary<string, string>
    {
        public string TypeName { get; set; }

        public NetsuiteCustomRecordDictionary(string TypeName) { this.TypeName = TypeName; }

        public NetsuiteCustomRecordDictionary(string TypeName, IEnumerable<CustomRecord> ListResults)
        {
            this.TypeName = TypeName;
            foreach (var r in ListResults)
                this.Add(r.name.ToLower(), r.internalId);
        }

        public int TotalValues
        {
            get { return this.Keys.Count(); }
        }
    }


}
