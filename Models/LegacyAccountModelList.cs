﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using static _2ndswingCommon.NetSuiteService;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace netsuite_fastsync
{
    public class LegacyAccountModelList : Dictionary<string, LegacyAccountMapModel>
    {
        NetSuiteHelper helper = new NetSuiteHelper();

        public LegacyAccountModelList()
        {
            CustomRecordSearch s = new CustomRecordSearch() {  basic = new CustomRecordSearchBasic() { recType = helper.CustomRecordRefForScriptId("customrecord_bf_legacymaps") }};
            SearchPreferences prefs = new SearchPreferences() { bodyFieldsOnly = false, pageSize = 100, pageSizeSpecified = true, returnSearchColumns = true };
            var models = helper.FindAll<CustomRecord>(s, prefs)
                .ToDictionary(r => r.name, r => new LegacyAccountMapModel(r));

            foreach (var model in models.Where(r => (r.Value != null && r.Value.Account != null)))
                this.Add(model.Key, model.Value);
        }
    }
}
