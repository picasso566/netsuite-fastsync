﻿using System;
using System.Collections.Generic;
using System.Text;

namespace netsuite_fastsync
{
    public class CounterpointUserPOCO : HashBasePOCO
    {
        public int IsGhostEmployee { get; set; }
        public string USR_ID { get; set; }
        public string NAM { get; set; }
        public string NAM_UPR { get; set; }
        public string INITIALS { get; set; }
        public string ADRS_1 { get; set; }
        public string ADRS_2 { get; set; }
        public string ADRS_3 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP_COD { get; set; }
        public string CNTRY { get; set; }
        public string CONTCT_1 { get; set; }
        public string CONTCT_2 { get; set; }
        public string PHONE_1 { get; set; }
        public string PHONE_2 { get; set; }
        public string FAX_1 { get; set; }
        public string FAX_2 { get; set; }
        public string EMAIL_ADRS_1 { get; set; }
        public string EMAIL_ADRS_2 { get; set; }
        public string URL_1 { get; set; }
        public string URL_2 { get; set; }
        public string WRKGRP_ID { get; set; }
        public string ALLOW_OTHR_WRKGRPS { get; set; }
        public string DEPT { get; set; }
        public string EMP_NO { get; set; }
        public string SEC_COD { get; set; }
        public string IS_PS_USR { get; set; }
        public string IS_PS_MGR { get; set; }
        public string IS_SLS_REP { get; set; }
        public string IS_BUYER { get; set; }
        public string LOGIN_DISABLED { get; set; }
        public string COMMIS_COD { get; set; }
        public string LST_MAINT_USR_ID { get; set; }
        public string IS_MAIL_MGR { get; set; }
        public string ALLOW_SEND_POPUPMAIL { get; set; }
        public string TA_GsvPassword { get; set; }
        public string TA_GsvPermission { get; set; }
        public string IS_OM_USR { get; set; }
        public byte? RS_STAT { get; set; }
        public string RPT_EMAIL { get; set; }
        public string MSR_LOGIN_ID_1 { get; set; }
        public string MSR_LOGIN_ID_2 { get; set; }
    }
}
