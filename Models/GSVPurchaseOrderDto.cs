﻿using System;
using System.Collections.Generic;
using System.Text;

namespace netsuite_fastsync
{
    public class GSVPurchaseOrderDto
    {
        public string CurrentHash { get; set; }
        public List<GSVPurchaseOrderLineDto> Lines { get; set; }

        public int PoHdrRecId { get; set; }
        public string PoNo { get; set; }
        public DateTime? PoDate { get; set; }
        public DateTime? StatusDate { get; set; }
        public string VendNo { get; set; }
        public string PoStatus { get; set; }
        public DateTime? DateReceived { get; set; }
        public string IsOkToPay { get; set; }
        public decimal? PaidCheckAmt { get; set; }
        public string PaidCheckDocNo { get; set; }
        public DateTime? PaidCheckDate { get; set; }
        public decimal? PaidPayPalAmt { get; set; }
        public string PaidPayPalDocNo { get; set; }
        public DateTime? PaidPayPalDate { get; set; }
        public decimal? PaidFirstTeeAmt { get; set; }
        public string PaidFirstTeeDocNo { get; set; }
        public DateTime? PaidFirstTeeDate { get; set; }
        public int? ShippingLabels { get; set; }
        public decimal? ShippingCharges { get; set; }
        public string IsFreeShipping { get; set; }
        public string Notes { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime DateCreated { get; set; }
        public int? PaymentApprovalId { get; set; }
        public decimal? RequestedCheckAmt { get; set; }
        public decimal? RequestedPayPalAmt { get; set; }
        public decimal? Requested1stTeeAmt { get; set; }
        public string UserId { get; set; }
        public string Source { get; set; }
        public string PmtAuth { get; set; }
        public string PmtAuthUser { get; set; }
        public string PromoCode { get; set; }
        public decimal? PromoAmt { get; set; }
        public string TagName { get; set; }
        public int? WebOrderId { get; set; }
        public int? PriceLevelId { get; set; }
        public Guid? SiteGUID { get; set; }
        public int? WidgetId { get; set; }
        public decimal? SitePriceAdjPct { get; set; }
        public string TradeOrExcess { get; set; }
        public string SentToPassPort { get; set; }
        public string CustNo { get; set; }
        public decimal? RequestedStoreCreditAmt { get; set; }
        public decimal? PaidStoreCreditAmt { get; set; }
        public string PaidStoreCreditDocNo { get; set; }
        public DateTime? PaidStoreCreditDate { get; set; }
        public DateTime? LolDateCreated { get; set; }
        public DateTime? LolDateUploaded { get; set; }
        public string LolErrorMsg { get; set; }
        public string InternalNotes { get; set; }
        public string ReceivingLocId { get; set; }
        public int vendorExists { get; set; }
        public int SiteGUIDExists { get; set; }
        public int UserExists { get; set; }

    }
}
