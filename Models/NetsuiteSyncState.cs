﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using static _2ndswingCommon.NetSuiteService;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace netsuite_fastsync
{

    public class NetsuiteSyncHashSet
    {
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string NSExternalId { get; set; }
        public string NSInternalId { get; set; }
        public string NetSuiteHash { get; set; }
    }

    public class NetSuiteSyncStateIds
    {
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string NSInternalId { get; set; }
        public string NSExternalId { get; set; }
    }

    public class NetSuiteSyncStateNoXML
    {
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string NSBatchName { get; set; }
        public string RecordType { get; set; }
        public string NetSuiteHash { get; set; }
        public bool NSSendSuccess { get; set; }
        public string NSAPIMessage { get; set; }
        public string NSInternalId { get; set; }
        public string NSExternalId { get; set; }
        public DateTime DateUpdatedInNs { get; set; }
        public string LegacyMongoId { get; set; }
    }

    public class NetSuiteSyncState
    {
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string NSBatchName { get; set; }
        public string RecordType { get; set; }
        public string NetSuiteHash { get; set; }
        public bool NSSendSuccess { get; set; }
        public string NSAPIMessage { get; set; }
        public string DataSent { get; set; }
        public string NSInternalId { get; set; }
        public string NSExternalId { get; set; }
        public DateTime DateUpdatedInNs { get; set; }
        public string LegacyMongoId { get; set; }
    }


    public class NetSuiteSyncInit
    {
        public string Id { get; set; }
        public string NSBatchName { get; set; }
        public string FromType { get; set; }
        public string ToType { get; set; }
        public string NetSuiteHash { get; set; }
        public bool NSSendSuccess { get; set; }
        public string NSAPIMessage { get; set; }
        public string DataSent { get; set; }
        public string NSInternalId { get; set; }
        public string NSExternalId { get; set; }
        public DateTime DateUpdatedInNs { get; set; }
    }

}
