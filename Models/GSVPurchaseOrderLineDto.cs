﻿using System;
using System.Collections.Generic;
using System.Text;

namespace netsuite_fastsync
{ 
    public class GSVPurchaseOrderLineDto
    {
        public string PoNo { get; set; }
        public int PoHdrRecId { get; set; }
        public int Line_PoLineSeqNo { get; set; }
        public string Line_TemplateItemNo { get; set; }
        public int? Line_ItemId { get; set; }
        public int? Line_ItemTypeId { get; set; }
        public string Line_Description { get; set; }
        public string Line_CpItemNo { get; set; }
        public int? Line_ExpClubsInIronSet { get; set; }
        public int? Line_RecvClubsInIronSet { get; set; }
        public decimal? Line_ClubLength { get; set; }
        public int? Line_ShaftFlexId { get; set; }
        public int? Line_QtyExpected { get; set; }
        public int? Line_ExpectedConditionId { get; set; }
        public int? Line_ReceivedConditionId { get; set; }
        public decimal? Line_ExpUnitPrice { get; set; }
        public decimal? Line_ExpectedPrice { get; set; }
        public decimal? Line_RecvUnitPrice { get; set; }
        public decimal? Line_ReceivedPrice { get; set; }
        public string Line_PoLineNotes { get; set; }
        public DateTime Line_DateCreated { get; set; }
        public int? Line_OrigPoLineSeqNo { get; set; }
        public int? Line_QtyReceived { get; set; }
        public string Line_UserId { get; set; }
        public string Line_NotOnPo { get; set; }
        public DateTime? Line_DateLastChanged { get; set; }
        public decimal? Line_BaseBidPrice { get; set; }
        public string Line_Cancel { get; set; }
        public int? Line_ExpClubNoId { get; set; }
        public int? Line_RecvClubNoId { get; set; }
        public int? Line_Old_ExpectedConditionId { get; set; }
        public int? Line_Old_ReceivedConditionId { get; set; }
        public string Line_BuyType { get; set; }
        public string Line_OpportunityBuy { get; set; }
        public string Line_ExpandOpportunityBuy { get; set; }
        public int vendorExists { get; set; }
        public int SiteGUIDExists { get; set; }
        public int UserExists { get; set; }
        public string externalItemId { get; set; }

    }
}
