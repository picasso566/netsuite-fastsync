﻿using System;
using System.Collections.Generic;
using System.Text;

namespace netsuite_fastsync
{
    public class ParentItemPOCO:HashBasePOCO
    {
        public string SKU { get; set; }
        public string name { get; set; }
        public string Summary { get; set; }
        public string ExternalId { get; set; }
        public int BrandId { get; set; }
        public string ItemId { get; set; }
        public string salesDescription { get; set; }
        public string storeDecription { get; set; }
        public decimal? BidPrice { get; set; }
        public int? ItemTypeId { get; set; }
        public string ShaftMatl { get; set; }
        public decimal? RetailNewPrice { get; set; }
        public decimal? SalesRepPrice { get; set; }
        public string OkForSf { get; set; }
        public string OkForAz { get; set; }
        public string OkForEb { get; set; }
        public string OkForStores { get; set; }
        public string ListOnSf { get; set; }
        public string GsProductGroupCode { get; set; }
        public string ListOnEbay { get; set; }
        public string ListOnAmazon { get; set; }
        public int CategoryId { get; set; }
        public int ModelId { get; set; }
        public Guid ProductGUID { get; set; }
        public string TemplateModelSummary { get; set; }
        public string Description { get; set; }
        public byte Published { get; set; }
        public string RelatedProducts { get; set; }
        public byte IsAKit { get; set; }
        public byte Deleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public int PageSize { get; set; }
        public string RelatedBlogs { get; set; }
        public string ClubHeadSize { get; set; }
        public string ClubSpinRate { get; set; }
        public string ClubYearReleased { get; set; }
        public string ClubManufacturingType { get; set; }
        public string Forgiveness { get; set; }
        public string PlayerLevel { get; set; }
        public string TechConstruction { get; set; }
        public byte? IsConfiguration { get; set; }
        public string IronSetClubs { get; set; }
        public string ToeHang { get; set; }
        public string PutterHeadStyle { get; set; }
        public string ClubDesiredBallFlight { get; set; }
        public Guid? UsedKitGUID { get; set; }
        public int UsedKitExists { get; set; }
        public Guid? NewKitGUID { get; set; }
        public int NewKitExists { get; set; }
        public string ForgedOrCast { get; set; }
        public string Torque { get; set; }
        public string Weight { get; set; }
        public string TipDiameter { get; set; }
        public string Launch { get; set; }
        public string DefaultShaft_1 { get; set; }
        public string DefaultShaft_2 { get; set; }
        public string DefaultShaft_3 { get; set; }
        public string DefaultShaft_4 { get; set; }
        public string DefaultShaft_5 { get; set; }
        public string StockVideoURL_01 { get; set; }
        public string ProductVideoURL_01 { get; set; }
        public string ProdSynonyms { get; set; }
        public string NsSKU { get; set; }
        public int? NsInternalId { get; set; }
        public string NsExternalId { get; set; }
        public string EndOfLife { get; set; }
    }
}
