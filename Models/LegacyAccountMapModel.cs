﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static _2ndswingCommon.NetSuiteService;

namespace netsuite_fastsync
{
    public class LegacyAccountMapModel
    {
        public RecordRef Account { get; set; }
        public RecordRef Location { get; set; }
        public RecordRef Department { get; set; }

        public LegacyAccountMapModel(CustomRecord LegacyMapRecord)
        {
            if (LegacyMapRecord.customFieldList != null &&
                LegacyMapRecord.customFieldList.Where(cf => cf.scriptId != null && cf.scriptId == "custrecord_bf_legacymapaccount").Count() > 0)
            {
                Account = GetRefForCustomRecord(LegacyMapRecord, RecordType.account, "custrecord_bf_legacymapaccount");
                Location = GetRefForCustomRecord(LegacyMapRecord, RecordType.location, "custrecord_bf_legacymaplocation");
                Department = GetRefForCustomRecord(LegacyMapRecord, RecordType.department, "custrecord_bf_legacymapdepartment");
            }
        }

        private RecordRef GetRefForCustomRecord(CustomRecord record, RecordType recType, string scriptId)
        {
            foreach (var cf in record.customFieldList.Where(cf => cf.scriptId != null && cf.scriptId == scriptId))
                if(cf != null && ((SelectCustomFieldRef)cf) != null)
                    return GetRef(recType, ((SelectCustomFieldRef)cf).value.internalId);
            return null;            
        }

        private RecordRef GetRef(RecordType recType, string internalId)
        {
            return new RecordRef()
            {
                type = recType,
                typeSpecified = true,
                internalId = internalId
            };
        }
    }
}



