﻿using System;
using System.Collections.Generic;
using System.Text;

namespace netsuite_fastsync
{
    public class NewItemPOCO : HashBasePOCO
    {
        public string NsParentItemSku { get; set; }
        public string GsProductGroupCode { get; set; }
        public int ItemId { get; set; }
        public string TemplateItemNo { get; set; }
        public string CpItemNo { get; set; }
        public string UPC { get; set; }
        public string Used { get; set; }
        public int? ItemTypeId { get; set; }
        public int? CategoryId { get; set; }
        public int? SubCategoryId { get; set; }
        public string Description { get; set; }
        public string LongDescription { get; set; }
        public string StockDescription { get; set; }
        public string OnlineTitle { get; set; }
        public int? BounceId { get; set; }
        public int? BrandId { get; set; }
        public int? NumberOfClubsInIronSet { get; set; }
        public int? ConditionId { get; set; }
        public int? DexterityId { get; set; }
        public string FreeNotes { get; set; }
        public decimal? Length { get; set; }
        public int? LieAngleId { get; set; }
        public decimal? Loft { get; set; }
        public int? LocationId { get; set; }
        public int? ModelId { get; set; }
        public int? ShaftFlexId { get; set; }
        public int? ShaftTypeId { get; set; }
        public decimal? SellPrice { get; set; }
        public decimal? BidPrice { get; set; }
        public decimal? PricePremiumAmt { get; set; }
        public decimal? PricePremiumPct { get; set; }
        public DateTime? PricePremiumApplyDate { get; set; }
        public decimal? PriceDiscountAmt { get; set; }
        public decimal? PriceDiscountPct { get; set; }
        public DateTime? PriceDiscountApplyDate { get; set; }
        public string UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public string MfrPriceRuleCode { get; set; }
        public string OkForSf { get; set; }
        public string OkForAz { get; set; }
        public string OkForEb { get; set; }
        public Guid ItemGUID { get; set; }
        public string Source { get; set; }
        public string ShaftMatl { get; set; }
        public int? TemplateId { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
        public int? YearRelease { get; set; }
        public string OkForStores { get; set; }
        public string ClubDesiredBallFlight { get; set; }
        public string Gripped { get; set; }
        public string Torque { get; set; }
        public string Weight { get; set; }
        public string Spin { get; set; }
        public string Launch { get; set; }
        public string ShaftClubType { get; set; }
        public string Color { get; set; }
        public string BagSize { get; set; }
        public string Size { get; set; }
        public string Gender { get; set; }
        public string Fit { get; set; }
        public string Style { get; set; }
        public string StyleNo { get; set; }
        public string Material { get; set; }
        public string Pattern { get; set; }
        public string NewLength { get; set; }
        public string Width { get; set; }
        public string Spikes { get; set; }
        public string ShaftFlex { get; set; }
        public string Bounce { get; set; }
        public string ShaftType { get; set; }
        public string Dexterity { get; set; }
        public string LieAngle { get; set; }
        public string ClubNo { get; set; }
        public string RibbedOrRound { get; set; }
        public int? NumberOfGolfBalls { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Condition { get; set; }
        public string Location { get; set; }
        public string SubCategory { get; set; }
        public string IsEcommerceItem { get; set; }
        public string EbayTitle { get; set; }
        public decimal? ClearancePrice { get; set; }
        public string BuyType { get; set; }
        public string OpportunityBuy { get; set; }
        public decimal? OutOfStateSalePrice { get; set; }
        public decimal? MSRP { get; set; }
        public string ChangedBy { get; set; }
        public DateTime? DateChanged { get; set; }
        public string ListOn2SOnly { get; set; }
        public string Grind { get; set; }
        public string HasImage { get; set; }
        public string FormattedLength { get; set; }
        public string FormattedLoft { get; set; }
    }
}
