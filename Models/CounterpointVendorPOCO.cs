﻿using System;
using System.Collections.Generic;
using System.Text;

namespace netsuite_fastsync
{
    class CounterpointVendorPOCO:HashBasePOCO
    {
        public string vend_no { get; set; }
        public string nam { get; set; }
        public string fst_nam { get; set; }
        public string lst_nam { get; set; }
        public string categ_cod { get; set; }
        public string salutation { get; set; }
        public string adrs_1 { get; set; }
        public string adrs_2 { get; set; }
        public string adrs_3 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip_cod { get; set; }
        public string cntry { get; set; }
        public string phone_1 { get; set; }
        public string phone_2 { get; set; }
        public string contct_1 { get; set; }
        public string contct_2 { get; set; }
        public string email_adrs_1 { get; set; }
        public string email_adrs_2 { get; set; }
        public DateTime? prof_dat_1 { get; set; }
        public string SiteGUID { get; set; }
    }
}
