﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace netsuite_fastsync
{
    public class CounterpointCustomerAddressPOCO: HashBasePOCO
    {
        public bool IsDefault { get; set; }

        public string CustomerId { get; set; }
        public int? AddressId { get; set; }
        public string Country { get; set; }
        public string Addressee { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
