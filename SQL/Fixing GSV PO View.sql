USE [SecondSyncState]
GO



ALTER VIEW [dbo].[NetSuite_VI_GSVPOHeaderLinesJoined]
AS

SELECT
	h.[PoHdrRecId]
	,LTRIM(RTRIM(h.[PoNo])) PoNo
	,h.[PoDate]
	,h.[StatusDate]
	,h.[VendNo]
	,h.[PoStatus]
	,h.[DateReceived]
	,h.[IsOkToPay]
	,h.[PaidCheckAmt]
	,h.[PaidCheckDocNo]
	,h.[PaidCheckDate]
	,h.[PaidPayPalAmt]
	,h.[PaidPayPalDocNo]
	,h.[PaidPayPalDate]
	,h.[PaidFirstTeeAmt]
	,h.[PaidFirstTeeDocNo]
	,h.[PaidFirstTeeDate]
	,h.[ShippingLabels]
	,h.[ShippingCharges]
	,h.[IsFreeShipping]
	,h.[Notes]
	,h.[ErrorMessage]
	,h.[DateCreated]
	,h.[PaymentApprovalId]
	,h.[RequestedCheckAmt]
	,h.[RequestedPayPalAmt]
	,h.[Requested1stTeeAmt]
	,h.[UserId]
	,h.[Source]
	,h.[PmtAuth]
	,h.[PmtAuthUser]
	,h.[PromoCode]
	,h.[PromoAmt]
	,h.[TagName]
	,h.[WebOrderId]
	,pl.PriceLevelId
	,h.[SiteGUID]
	,h.[WidgetId]
	,h.[SitePriceAdjPct]
	,h.[TradeOrExcess]
	,ISNULL( h.[SentToPassPort] , 0) SentToPassPort
	,h.[CustNo]
	,h.[RequestedStoreCreditAmt]
	,h.[PaidStoreCreditAmt]
	,h.[PaidStoreCreditDocNo]
	,h.[PaidStoreCreditDate]
	,h.[LolDateCreated]
	,h.[LolDateUploaded]
	,h.[LolErrorMsg]
	,h.[InternalNotes]
	,h.[ReceivingLocId]
	,h.SitePayCode -- 08/13/18
	,h.SitePayCodeAdjPct  -- 08/13/18
	,h.RequestedAmt -- 08/13/18
	,h.GiftCardGUID -- 08/13/18


	,l.[PoLineSeqNo] AS Line_PoLineSeqNo
	,l.[TemplateItemNo] AS Line_TemplateItemNo
	,l.[ItemId] AS Line_ItemId
	,l.[ItemTypeId] AS Line_ItemTypeId
	,l.[Description] AS Line_Description
	,l.[CpItemNo] AS Line_CpItemNo
	,l.[ExpClubsInIronSet] AS Line_ExpClubsInIronSet
	,l.[RecvClubsInIronSet] AS Line_RecvClubsInIronSet
	,l.[ClubLength] AS Line_ClubLength
	,l.[ShaftFlexId] AS Line_ShaftFlexId
	,l.[QtyExpected] AS Line_QtyExpected
	,l.ExpectedConditionId AS Line_ExpectedConditionId
	,l.ReceivedConditionId AS Line_ReceivedConditionId
	,l.[ExpUnitPrice] AS Line_ExpUnitPrice
	,l.[ExpectedPrice] AS Line_ExpectedPrice
	,l.[RecvUnitPrice] AS Line_RecvUnitPrice
	,l.[ReceivedPrice] AS Line_ReceivedPrice
	,l.[PoLineNotes] AS Line_PoLineNotes
	,l.[DateCreated] AS Line_DateCreated
	,l.[OrigPoLineSeqNo] AS Line_OrigPoLineSeqNo
	,l.[QtyReceived] AS Line_QtyReceived
	,l.[UserId] AS Line_UserId
	,l.[NotOnPo] AS Line_NotOnPo
	,l.[DateLastChanged] AS Line_DateLastChanged
	,l.[BaseBidPrice] AS Line_BaseBidPrice
	,l.[Cancel] AS Line_Cancel
	,l.[ExpClubNoId] AS Line_ExpClubNoId
	,l.[RecvClubNoId] AS Line_RecvClubNoId
	,l.[Old_ExpectedConditionId] AS Line_Old_ExpectedConditionId
	,l.[Old_ReceivedConditionId] AS Line_Old_ReceivedConditionId
	,l.[BuyType] AS Line_BuyType
	,l.[OpportunityBuy] AS Line_OpportunityBuy
	,l.[ExpandOpportunityBuy] AS Line_ExpandOpportunityBuy
	
	,CASE WHEN vensync.Id IS NOT NULL THEN 1 ELSE 0 END AS vendorExists
	,CASE WHEN 
		SiteGUID = '00000000-0000-0000-0000-000000000000' OR 
		SiteGUID IS NULL OR
		sitesync.Id IS NOT NULL THEN 1 ELSE 0 END AS SiteGUIDExists
	,CASE WHEN empsync.Id IS NOT NULL THEN 1 ELSE 0 END AS UserExists

	,skuMap.NsParentItemSku
	,skuMap2.NsParentItemSku

FROM [dbo].[PoHdr] h
	INNER JOIN [dbo].[PoLine] l ON l.[PoHdrRecId] = h.[PoHdrRecId]
	LEFT JOIN dbo.PriceLevel pl ON pl.PriceLevelId = h.PriceLevelId
	LEFT JOIN dbo.NetsuiteSyncstate vensync ON vensync.RecordType = 'vendor'
							AND vensync.NSBatchName = 'vendors'
							AND vensync.LocalId = h.vendno 
							AND vensync.NSHadSuccess = 1
	LEFT JOIN dbo.NetsuiteSyncstate sitesync ON sitesync.RecordType LIKE 'CustomRecord' 
							AND sitesync.NSHadSuccess = 1
							AND sitesync.nsbatchname LIKE 'gsvSites1'
							AND dbo.FixLocalId(sitesync.LocalId) = h.SiteGUID
	LEFT JOIN dbo.NetsuiteSyncstate empsync ON empsync.RecordType LIKE 'employee' 
							AND empsync.NSHadSuccess = 1
							AND empsync.nsbatchname LIKE 'employees'
							AND empsync.LocalId = h.UserId

LEFT JOIN ViNsSkuFromTemplateItemNo skuMap on skuMap.TemplateItemNo = l.TemplateItemNo
LEFT JOIN ViNsSkuFromTemplateItemNo skuMap2 on skuMap2.TemplateItemNo = l.CpItemNo

GO




--select top 5000 UserId, TradeOrExcess, SiteGUID, dbo.FixLocalId(SiteGUID), SiteGUIDExists, * from NetSuite_VI_GSVPOHeaderLinesJoined
--where SiteGUIDExists = 1


