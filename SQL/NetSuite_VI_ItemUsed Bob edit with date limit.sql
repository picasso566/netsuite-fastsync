USE [SecondSyncState]
GO

/****** Object:  View [dbo].[NetSuite_VI_ItemUsed]    Script Date: 8/9/2018 3:48:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
08/01/18 BF Create for Used Item QuickSync
20180805 PP Added formatted loft and length fields
*/
ALTER VIEW [dbo].[NetSuite_VI_ItemUsed]
AS
    SELECT TIO.NsParentItemSku ,
           Cat.GsProductGroupCode ,
           I.ItemId ,
           I.TemplateItemNo ,
           I.CpItemNo ,
           I.UPC ,
           I.Used ,
           I.ItemTypeId ,
           I.CategoryId ,
           I.SubCategoryId ,
           I.[Description] ,
           I.LongDescription ,
           I.StockDescription ,
           I.OnlineTitle ,
           I.BounceId ,
           I.BrandId ,
           I.NumberOfClubsInIronSet ,
           cond.AttrId AS ConditionId ,
           I.DexterityId ,
           I.FreeNotes ,
           I.[Length] ,
           I.LieAngleId ,
           I.Loft ,
           I.LocationId ,
           I.ModelId ,
           I.ShaftFlexId ,
           I.ShaftTypeId ,
           I.SellPrice ,
           I.BidPrice ,
           I.PricePremiumAmt ,
           I.PricePremiumPct ,
           I.PricePremiumApplyDate ,
           I.PriceDiscountAmt ,
           I.PriceDiscountPct ,
           I.PriceDiscountApplyDate ,
           I.UserId ,
           I.DateCreated ,
           I.MfrPriceRuleCode ,
           I.OkForSf ,
           I.OkForAz ,
           I.OkForEb ,
           I.ItemGUID ,
           I.Source ,
           I.ShaftMatl ,
           I.TemplateId ,
           I.Price ,
           I.Cost ,
           I.YearRelease ,
           I.OkForStores ,
           I.ClubDesiredBallFlight ,
           I.Gripped ,
           I.Torque ,
           I.[Weight] ,
           I.Spin ,
           I.Launch ,
           I.ShaftClubType ,
           I.Color ,
           I.BagSize ,
           I.Size ,
           I.Gender ,
           I.Fit ,
           I.Style ,
           I.StyleNo ,
           I.Material ,
           I.Pattern ,
           I.NewLength ,
           I.Width ,
           I.Spikes ,
           I.ShaftFlex ,
           I.Bounce ,
           I.ShaftType ,
           I.Dexterity ,
           I.LieAngle ,
           I.ClubNo ,
           I.RibbedOrRound ,
           I.NumberOfGolfBalls ,
           I.Category ,
           I.Brand ,
           I.Model ,
           I.Condition ,
           I.[Location] ,
           I.SubCategory ,
           I.IsEcommerceItem ,
           I.EbayTitle ,
           I.ClearancePrice ,
           I.BuyType ,
           I.OpportunityBuy ,
           I.OutOfStateSalePrice ,
           I.MSRP ,
           I.ChangedBy ,
           I.DateChanged ,
           I.ListOn2SOnly ,
           I.Grind ,
           I.HasImage ,
           CASE WHEN I.[Length] IS NOT NULL
                     AND I.[Length] > 0
                     AND validLengths.ShaftLength IS NULL THEN
                    'Invalid Length'
                ELSE validLengths.FormattedLength
           END AS FormattedLength ,
           CASE WHEN validLofts.loft IS NOT NULL THEN
                    validLofts.loftformatted
                ELSE ''
           END AS FormattedLoft
    FROM   Item I
           INNER JOIN ViNsSkuFromTemplateItemNo TIO ON TIO.TemplateItemNo = I.TemplateItemNo
           INNER JOIN Category Cat ON Cat.CategoryId = I.CategoryId
           LEFT JOIN (   SELECT   ItemId ,
                                  MAX(TrxDateTime) MaxTrxDateTime
                         FROM     ItemChangeLog_New
                         GROUP BY ItemId
                         HAVING   MAX(TrxDateTime) > DATEADD(WEEK, -1, GETDATE())
                     ) X ON X.ItemId = I.ItemId
           LEFT JOIN dbo.fnValidClubShaftLengths() validLengths ON CAST(validLengths.ShaftLength AS VARCHAR(10)) = ISNULL(
                                                                                                                             I.[Length] ,
                                                                                                                             '0'
                                                                                                                         )
           LEFT JOIN dbo.fnValidLoftValues() validLofts ON CAST(validLofts.loft AS VARCHAR(10)) = ISNULL(
                                                                                                            I.Loft ,
                                                                                                            '0'
                                                                                                        )
           LEFT OUTER JOIN dbo.Condition AS cond ON cond.ConditionId = I.ConditionId
    WHERE  I.Used = 'Y';

GO



SELECT distinct top 1000 ItemId FROM ItemChangeLog_New Where TrxDateTime > DATEADD(day, -7, GETDATE())