USE [SecondSyncState]
GO

/****** Object:  View [dbo].[NetSuite_VI_ParentItem]    Script Date: 8/8/2018 10:17:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
08/02/18 BF Created as a starting place for ParentItem quick sync
            Paul will need to compare with C# to make sure we're 
			handling the same.
SELECT TOP 1000 * FROM NetSuite_VI_ParentItem
*/
CREATE VIEW [dbo].[NetSuite_VI_ParentItem]
AS
    SELECT TM.SKU ,
           CASE WHEN TM.Name IS NULL THEN TM.SKU
                ELSE TM.SKU + ':' + SUBSTRING(TM.Name, 1, 60)
           END AS [name] ,
           ISNULL(T.Description, TM.Summary) AS Summary ,
           TM.NsExternalId AS ExternalId ,
           TM.BrandId ,
           TM.SKU AS ItemId ,
           SUBSTRING(TM.Summary, 1, 998) AS salesDescription ,
           T.Description AS storeDecription ,
           T.BidPrice ,
           T.ItemTypeId ,
           T.ShaftMatl ,
           T.RetailNewPrice ,
           T.SalesRepPrice ,
           T.OkForSf ,
           T.OkForAz ,
           T.OkForEb ,
		   T.OkForStores,
           CASE WHEN OH.Status = 'W' THEN 'Y'
                ELSE 'N'
           END AS ListOnSf ,
           C.GsProductGroupCode ,
           OH.ListOnEbay ,
           OH.ListOnAmazon ,
           TM.CategoryId ,
           TM.ModelId ,
           TM.ProductGUID ,
           TM.Summary TemplateModelSummary ,
           TM.Description ,
           TM.Published ,
           TM.RelatedProducts ,
           TM.IsAKit ,
           TM.Deleted ,
           TM.CreatedOn ,
           TM.PageSize ,
           TM.RelatedBlogs ,
           TM.ClubHeadSize ,
           TM.ClubSpinRate ,
           TM.ClubYearReleased ,
           TM.ClubManufacturingType ,
           TM.Forgiveness ,
           TM.PlayerLevel ,
           TM.TechConstruction ,
           TM.IsConfiguration ,
           TM.IronSetClubs ,
           TM.ToeHang ,
           TM.PutterHeadStyle ,
           TM.ClubDesiredBallFlight ,
           TM.UsedKitGUID ,
		   case when UsedKitSync.Id is not null then 1 else 0 end as UsedKitExists,
           TM.NewKitGUID ,
		   case when NewKitSync.Id is not null then 1 else 0 end as NewKitExists,
           TM.ForgedOrCast ,
           TM.Torque ,
           TM.Weight ,
           TM.TipDiameter ,
           TM.Launch ,
           TM.DefaultShaft_1 ,
           TM.DefaultShaft_2 ,
           TM.DefaultShaft_3 ,
           TM.DefaultShaft_4 ,
           TM.DefaultShaft_5 ,
           TM.StockVideoURL_01 ,
           TM.ProductVideoURL_01 ,
           TM.ProdSynonyms ,
           TM.NsSKU ,
           TM.NsInternalId ,
           TM.NsExternalId
    FROM   TemplateModel TM
           LEFT JOIN Template T ON T.CpItemNo = TM.NsExternalId
           LEFT JOIN ComaV2_stage2.dbo.TACOMA_OptionHdr OH ON OH.ParentItemNo = TM.NsExternalId
           LEFT JOIN Category C ON C.CategoryId = TM.CategoryId
		   LEFT JOIN NetSuiteSyncState UsedKitSync ON UsedKitSync.nsbatchname = 'kits' 
				AND UsedKitSync.nssendsuccess = 1 AND UsedKitSync.LocalId = CONVERT(VARCHAR(36), TM.UsedKitGUID) 
		   LEFT JOIN NetSuiteSyncState NewKitSync ON NewKitSync.nsbatchname = 'kits' 
				AND NewKitSync.nssendsuccess = 1 AND NewKitSync.LocalId = CONVERT(VARCHAR(36), TM.NewKitGUID) 
GO


