USE [SecondSyncState]
GO

if exists (select * from sys.objects where name = 'FixLocalId')
	drop function FixLocalId
go

/*********************************************************************************************************************
PP 4/12/2018 - Reformat Doc_no/Invoice numbers to remove 00 prefixes
*********************************************************************************************************************/
CREATE FUNCTION [dbo].[FixLocalId]
(
   @input varchar(250)
)
returns varchar(250)
AS
begin

declare @ret varchar(250)
if(charindex(':',@input) = 0 )
	set @ret = @input
else
	set @ret = ltrim(rtrim(substring(@input, charindex(':', @input) + 1 , (len(@input) + 1) - charindex(':', @input))))  
	
return @ret

end
GO

