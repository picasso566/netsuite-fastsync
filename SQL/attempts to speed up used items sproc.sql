--USE SecondSyncState 
--go 

--if exists  (select * from sys.procedures where name = 'NetSuiteGetChangedUsedItemsGEN') 
--drop procedure NetSuiteGetChangedUsedItemsGEN 
--go 


--CREATE PROCEDURE NetSuiteGetChangedUsedItemsGEN 
--AS
--SET NOCOUNT ON;

--IF OBJECT_ID('tempdb.dbo.#synced', 'U') IS NOT NULL 
--DROP TABLE #synced;

--create table #synced(LocalId varchar(250), LocalHash varchar(32))
--insert #synced(LocalId, LocalHash )
--SELECT LocalId, LocalHash  
--FROM NetSuiteSyncState WHERE RecordType = 'inventoryItem' and NSSendSuccess = 1 and NSBatchName = 'productUsedVariant2'


SELECT 
tbl.CpItemNo as LocalId, 
'productUsedVariant2' AS NSBatchName, 
'inventoryItem' AS RecordType, 

 dbo.fnHashString( 
     ISNULL(tbl.[NsParentItemSku], '') +
     ISNULL(tbl.[GsProductGroupCode], '') +
     CASE WHEN tbl.[ItemId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ItemId] ) END +
     ISNULL(tbl.[TemplateItemNo], '') +
     ISNULL(tbl.[CpItemNo], '') +
     ISNULL(tbl.[UPC], '') +
     ISNULL(tbl.[Used], '') +
     CASE WHEN tbl.[ItemTypeId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ItemTypeId] ) END +
     CASE WHEN tbl.[CategoryId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[CategoryId] ) END +
     CASE WHEN tbl.[SubCategoryId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[SubCategoryId] ) END +
     ISNULL(tbl.[Description], '') +
     ISNULL(tbl.[LongDescription], '') +
     ISNULL(tbl.[StockDescription], '') +
     ISNULL(tbl.[OnlineTitle], '') +
     CASE WHEN tbl.[BounceId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[BounceId] ) END +
     CASE WHEN tbl.[BrandId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[BrandId] ) END +
     CASE WHEN tbl.[NumberOfClubsInIronSet] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[NumberOfClubsInIronSet] ) END +
     CASE WHEN tbl.[ConditionId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ConditionId] ) END +
     CASE WHEN tbl.[DexterityId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[DexterityId] ) END +
     ISNULL(tbl.[FreeNotes], '') +
     CASE WHEN tbl.[Length] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Length] ) END +
     CASE WHEN tbl.[LieAngleId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[LieAngleId] ) END +
     CASE WHEN tbl.[Loft] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Loft] ) END +
     CASE WHEN tbl.[LocationId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[LocationId] ) END +
     CASE WHEN tbl.[ModelId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ModelId] ) END +
     CASE WHEN tbl.[ShaftFlexId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ShaftFlexId] ) END +
     CASE WHEN tbl.[ShaftTypeId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ShaftTypeId] ) END +
     CASE WHEN tbl.[SellPrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[SellPrice] ) END +
     CASE WHEN tbl.[BidPrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[BidPrice] ) END +
     CASE WHEN tbl.[PricePremiumAmt] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PricePremiumAmt] ) END +
     CASE WHEN tbl.[PricePremiumPct] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PricePremiumPct] ) END +
     CASE WHEN tbl.[PricePremiumApplyDate] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PricePremiumApplyDate] ) END +
     CASE WHEN tbl.[PriceDiscountAmt] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PriceDiscountAmt] ) END +
     CASE WHEN tbl.[PriceDiscountPct] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PriceDiscountPct] ) END +
     CASE WHEN tbl.[PriceDiscountApplyDate] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PriceDiscountApplyDate] ) END +
     ISNULL(tbl.[UserId], '') +
     CASE WHEN tbl.[DateCreated] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[DateCreated] ) END +
     ISNULL(tbl.[MfrPriceRuleCode], '') +
     ISNULL(tbl.[OkForSf], '') +
     ISNULL(tbl.[OkForAz], '') +
     ISNULL(tbl.[OkForEb], '') +
     CASE WHEN tbl.[ItemGUID] IS NULL THEN '' ELSE CONVERT( VARCHAR(50) , tbl.[ItemGUID] ) END +
     ISNULL(tbl.[Source], '') +
     ISNULL(tbl.[ShaftMatl], '') +
     CASE WHEN tbl.[TemplateId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[TemplateId] ) END +
     CASE WHEN tbl.[Price] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Price] ) END +
     CASE WHEN tbl.[Cost] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Cost] ) END +
     CASE WHEN tbl.[YearRelease] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[YearRelease] ) END +
     ISNULL(tbl.[OkForStores], '') +
     ISNULL(tbl.[ClubDesiredBallFlight], '') +
     ISNULL(tbl.[Gripped], '') +
     ISNULL(tbl.[Torque], '') +
     ISNULL(tbl.[Weight], '') +
     ISNULL(tbl.[Spin], '') +
     ISNULL(tbl.[Launch], '') +
     ISNULL(tbl.[ShaftClubType], '') +
     ISNULL(tbl.[Color], '') +
     ISNULL(tbl.[BagSize], '') +
     ISNULL(tbl.[Size], '') +
     ISNULL(tbl.[Gender], '') +
     ISNULL(tbl.[Fit], '') +
     ISNULL(tbl.[Style], '') +
     ISNULL(tbl.[StyleNo], '') +
     ISNULL(tbl.[Material], '') +
     ISNULL(tbl.[Pattern], '') +
     ISNULL(tbl.[NewLength], '') +
     ISNULL(tbl.[Width], '') +
     ISNULL(tbl.[Spikes], '') +
     ISNULL(tbl.[ShaftFlex], '') +
     ISNULL(tbl.[Bounce], '') +
     ISNULL(tbl.[ShaftType], '') +
     ISNULL(tbl.[Dexterity], '') +
     ISNULL(tbl.[LieAngle], '') +
     ISNULL(tbl.[ClubNo], '') +
     ISNULL(tbl.[RibbedOrRound], '') +
     CASE WHEN tbl.[NumberOfGolfBalls] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[NumberOfGolfBalls] ) END +
     ISNULL(tbl.[Category], '') +
     ISNULL(tbl.[Brand], '') +
     ISNULL(tbl.[Model], '') +
     ISNULL(tbl.[Condition], '') +
     ISNULL(tbl.[Location], '') +
     ISNULL(tbl.[SubCategory], '') +
     ISNULL(tbl.[IsEcommerceItem], '') +
     ISNULL(tbl.[EbayTitle], '') +
     CASE WHEN tbl.[ClearancePrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ClearancePrice] ) END +
     ISNULL(tbl.[BuyType], '') +
     ISNULL(tbl.[OpportunityBuy], '') +
     CASE WHEN tbl.[OutOfStateSalePrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[OutOfStateSalePrice] ) END +
     CASE WHEN tbl.[MSRP] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[MSRP] ) END +
     ISNULL(tbl.[ChangedBy], '') +
     CASE WHEN tbl.[DateChanged] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[DateChanged] ) END +
     ISNULL(tbl.[ListOn2SOnly], '') +
     ISNULL(tbl.[Grind], '') +
     ISNULL(tbl.[HasImage], '') +
     ISNULL(tbl.[FormattedLength], '') +
     ISNULL(tbl.[FormattedLoft], '') 
 ) as LocalHash 

 from SecondSyncState.dbo.NetSuite_VI_ItemUsed tbl 


 left join netsuitesyncstate sync on sync.NSBatchName = 'productUsedVariant2' 
			and sync.RecordType = 'inventoryItem'
			and sync.LocalId = tbl.CpItemNo
			and sync.LocalHash = dbo.fnHashString(        ISNULL(tbl.[NsParentItemSku], '') +       ISNULL(tbl.[GsProductGroupCode], '') +       CASE WHEN tbl.[ItemId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ItemId] ) END +       ISNULL(tbl.[TemplateItemNo], '') +       ISNULL(tbl.[CpItemNo], '') +       ISNULL(tbl.[UPC], '') +       ISNULL(tbl.[Used], '') +       CASE WHEN tbl.[ItemTypeId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ItemTypeId] ) END +       CASE WHEN tbl.[CategoryId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[CategoryId] ) END +       CASE WHEN tbl.[SubCategoryId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[SubCategoryId] ) END +       ISNULL(tbl.[Description], '') +       ISNULL(tbl.[LongDescription], '') +       ISNULL(tbl.[StockDescription], '') +       ISNULL(tbl.[OnlineTitle], '') +       CASE WHEN tbl.[BounceId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[BounceId] ) END +       CASE WHEN tbl.[BrandId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[BrandId] ) END +       CASE WHEN tbl.[NumberOfClubsInIronSet] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[NumberOfClubsInIronSet] ) END +       CASE WHEN tbl.[ConditionId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ConditionId] ) END +       CASE WHEN tbl.[DexterityId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[DexterityId] ) END +       ISNULL(tbl.[FreeNotes], '') +       CASE WHEN tbl.[Length] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Length] ) END +       CASE WHEN tbl.[LieAngleId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[LieAngleId] ) END +       CASE WHEN tbl.[Loft] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Loft] ) END +       CASE WHEN tbl.[LocationId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[LocationId] ) END +       CASE WHEN tbl.[ModelId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ModelId] ) END +       CASE WHEN tbl.[ShaftFlexId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ShaftFlexId] ) END +       CASE WHEN tbl.[ShaftTypeId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ShaftTypeId] ) END +       CASE WHEN tbl.[SellPrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[SellPrice] ) END +       CASE WHEN tbl.[BidPrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[BidPrice] ) END +       CASE WHEN tbl.[PricePremiumAmt] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PricePremiumAmt] ) END +       CASE WHEN tbl.[PricePremiumPct] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PricePremiumPct] ) END +       CASE WHEN tbl.[PricePremiumApplyDate] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PricePremiumApplyDate] ) END +       CASE WHEN tbl.[PriceDiscountAmt] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PriceDiscountAmt] ) END +       CASE WHEN tbl.[PriceDiscountPct] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PriceDiscountPct] ) END +       CASE WHEN tbl.[PriceDiscountApplyDate] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[PriceDiscountApplyDate] ) END +       ISNULL(tbl.[UserId], '') +       CASE WHEN tbl.[DateCreated] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[DateCreated] ) END +       ISNULL(tbl.[MfrPriceRuleCode], '') +       ISNULL(tbl.[OkForSf], '') +       ISNULL(tbl.[OkForAz], '') +       ISNULL(tbl.[OkForEb], '') +       CASE WHEN tbl.[ItemGUID] IS NULL THEN '' ELSE CONVERT( VARCHAR(50) , tbl.[ItemGUID] ) END +       ISNULL(tbl.[Source], '') +       ISNULL(tbl.[ShaftMatl], '') +       CASE WHEN tbl.[TemplateId] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[TemplateId] ) END +       CASE WHEN tbl.[Price] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Price] ) END +       CASE WHEN tbl.[Cost] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[Cost] ) END +       CASE WHEN tbl.[YearRelease] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[YearRelease] ) END +       ISNULL(tbl.[OkForStores], '') +       ISNULL(tbl.[ClubDesiredBallFlight], '') +       ISNULL(tbl.[Gripped], '') +       ISNULL(tbl.[Torque], '') +       ISNULL(tbl.[Weight], '') +       ISNULL(tbl.[Spin], '') +       ISNULL(tbl.[Launch], '') +       ISNULL(tbl.[ShaftClubType], '') +       ISNULL(tbl.[Color], '') +       ISNULL(tbl.[BagSize], '') +       ISNULL(tbl.[Size], '') +       ISNULL(tbl.[Gender], '') +       ISNULL(tbl.[Fit], '') +       ISNULL(tbl.[Style], '') +       ISNULL(tbl.[StyleNo], '') +       ISNULL(tbl.[Material], '') +       ISNULL(tbl.[Pattern], '') +       ISNULL(tbl.[NewLength], '') +       ISNULL(tbl.[Width], '') +       ISNULL(tbl.[Spikes], '') +       ISNULL(tbl.[ShaftFlex], '') +       ISNULL(tbl.[Bounce], '') +       ISNULL(tbl.[ShaftType], '') +       ISNULL(tbl.[Dexterity], '') +       ISNULL(tbl.[LieAngle], '') +       ISNULL(tbl.[ClubNo], '') +       ISNULL(tbl.[RibbedOrRound], '') +       CASE WHEN tbl.[NumberOfGolfBalls] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[NumberOfGolfBalls] ) END +       ISNULL(tbl.[Category], '') +       ISNULL(tbl.[Brand], '') +       ISNULL(tbl.[Model], '') +       ISNULL(tbl.[Condition], '') +       ISNULL(tbl.[Location], '') +       ISNULL(tbl.[SubCategory], '') +       ISNULL(tbl.[IsEcommerceItem], '') +       ISNULL(tbl.[EbayTitle], '') +       CASE WHEN tbl.[ClearancePrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[ClearancePrice] ) END +       ISNULL(tbl.[BuyType], '') +       ISNULL(tbl.[OpportunityBuy], '') +       CASE WHEN tbl.[OutOfStateSalePrice] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[OutOfStateSalePrice] ) END +       CASE WHEN tbl.[MSRP] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[MSRP] ) END +       ISNULL(tbl.[ChangedBy], '') +       CASE WHEN tbl.[DateChanged] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), tbl.[DateChanged] ) END +       ISNULL(tbl.[ListOn2SOnly], '') +       ISNULL(tbl.[Grind], '') +       ISNULL(tbl.[HasImage], '') +       ISNULL(tbl.[FormattedLength], '') +       ISNULL(tbl.[FormattedLoft], '')    )
where 
sync.Id is null
and tbl.ItemId in (SELECT distinct ItemId FROM ItemChangeLog_New Where TrxDateTime > DATEADD(day, -7, GETDATE()))

--EXCEPT  
--	select LocalId, NSBatchName = 'productUsedVariant2', RecordType = 'inventoryItem', LocalHash from #synced




 --GO



 --exec NetSuiteGetChangedUsedItemsGEN