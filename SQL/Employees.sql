



if exists (select * from sys.views where name = 'VI_CP_Users')
DROP VIEW VI_CP_Users
GO

/*PP 20180731 Get all the VendorCategories not already uploaded*/
CREATE VIEW VI_CP_Users
AS

SELECT 
IsGhostEmployee = 0,
USR_ID,
NAM,
NAM_UPR,
INITIALS,
ADRS_1,
ADRS_2,
ADRS_3,
CITY,
[STATE],
ZIP_COD,
CNTRY,
CONTCT_1,
CONTCT_2,
PHONE_1,
PHONE_2,
FAX_1,
FAX_2,
EMAIL_ADRS_1,
EMAIL_ADRS_2,
URL_1,
URL_2,
WRKGRP_ID,
ALLOW_OTHR_WRKGRPS,
DEPT,
EMP_NO,
SEC_COD,
IS_PS_USR,
IS_PS_MGR,
IS_SLS_REP,
IS_BUYER,
LOGIN_DISABLED,
COMMIS_COD,
LST_MAINT_USR_ID,
IS_MAIL_MGR,
ALLOW_SEND_POPUPMAIL,
TA_GsvPassword,
TA_GsvPermission,
IS_OM_USR,
RS_STAT,
RPT_EMAIL,
MSR_LOGIN_ID_1,
MSR_LOGIN_ID_2
FROM dbo.SY_USR

union 

--Basic User info for users on POs but not in CP
select distinct 
IsGhostEmployee = 1,
UserId as USR_ID,
UserId as NAM,
UPPER(UserId) as NAM_UPR,
null as INITIALS,
null as ADRS_1,
null as ADRS_2,
null as ADRS_3,
null as CITY,
null as [STATE],
null as ZIP_COD,
null as CNTRY,
null as CONTCT_1,
null as CONTCT_2,
null as PHONE_1,
null as PHONE_2,
null as FAX_1,
null as FAX_2,
null as EMAIL_ADRS_1,
null as EMAIL_ADRS_2,
null as URL_1,
null as URL_2,
null as WRKGRP_ID,
null as ALLOW_OTHR_WRKGRPS,
null as DEPT,
null as EMP_NO,
null as SEC_COD,
null as IS_PS_USR,
null as IS_PS_MGR,
null as IS_SLS_REP,
null as IS_BUYER,
null as LOGIN_DISABLED,
null as COMMIS_COD,
null as LST_MAINT_USR_ID,
null as IS_MAIL_MGR,
null as ALLOW_SEND_POPUPMAIL,
null as TA_GsvPassword,
null as TA_GsvPermission,
null as IS_OM_USR,
null as RS_STAT,
null as RPT_EMAIL,
null as MSR_LOGIN_ID_1,
null as MSR_LOGIN_ID_2
from (
    select distinct upper(UserId) as UserId from PoHdr where UserId is not null
    union
    select distinct upper(UserId) from PoLine where UserId is not null
    union
    select distinct upper(PmtAuthuser) from PoHdr where PmtAuthuser is not null
) t 
where not t.UserId = '' 
	and t.UserId not in (select usr_id from dbo.SY_USR)



GO






USE SecondSyncState 
go 

if exists  (select * from sys.procedures where name = 'NetSuiteGetChangedEmployeesGEN') 
drop procedure NetSuiteGetChangedEmployeesGEN 
go 


CREATE PROCEDURE NetSuiteGetChangedEmployeesGEN 
AS
SET NOCOUNT ON;
SELECT DISTINCT 
USR_ID as LocalId, 
'employee' AS RecordType
, CONVERT(VARCHAR(32), HASHBYTES('MD5' ,
     CASE WHEN [IsGhostEmployee] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), [IsGhostEmployee] ) END +
     ISNULL([USR_ID], '') +
     ISNULL([NAM], '') +
     ISNULL([NAM_UPR], '') +
     ISNULL([INITIALS], '') +
     ISNULL([ADRS_1], '') +
     ISNULL([ADRS_2], '') +
     ISNULL([ADRS_3], '') +
     ISNULL([CITY], '') +
     ISNULL([STATE], '') +
     ISNULL([ZIP_COD], '') +
     ISNULL([CNTRY], '') +
     ISNULL([CONTCT_1], '') +
     ISNULL([CONTCT_2], '') +
     ISNULL([PHONE_1], '') +
     ISNULL([PHONE_2], '') +
     ISNULL([FAX_1], '') +
     ISNULL([FAX_2], '') +
     ISNULL([EMAIL_ADRS_1], '') +
     ISNULL([EMAIL_ADRS_2], '') +
     ISNULL([URL_1], '') +
     ISNULL([URL_2], '') +
     ISNULL([WRKGRP_ID], '') +
     ISNULL([ALLOW_OTHR_WRKGRPS], '') +
     ISNULL([DEPT], '') +
     ISNULL([EMP_NO], '') +
     ISNULL([SEC_COD], '') +
     ISNULL([IS_PS_USR], '') +
     ISNULL([IS_PS_MGR], '') +
     ISNULL([IS_SLS_REP], '') +
     ISNULL([IS_BUYER], '') +
     ISNULL([LOGIN_DISABLED], '') +
     ISNULL([COMMIS_COD], '') +
     ISNULL([LST_MAINT_USR_ID], '') +
     ISNULL([IS_MAIL_MGR], '') +
     ISNULL([ALLOW_SEND_POPUPMAIL], '') +
     ISNULL([TA_GsvPassword], '') +
     ISNULL([TA_GsvPermission], '') +
     ISNULL([IS_OM_USR], '') +
     CASE WHEN [RS_STAT] IS NULL THEN '' ELSE CONVERT( VARCHAR(20), [RS_STAT] ) END +
     ISNULL([RPT_EMAIL], '') +
     ISNULL([MSR_LOGIN_ID_1], '') +
     ISNULL([MSR_LOGIN_ID_2], '') 
), 2 ) as LocalHash 
 from SecondSyncState.dbo.VI_CP_Users 
EXCEPT 
SELECT LocalId, 'employee' AS RecordType, LocalHash 
FROM SecondSyncState.dbo.NetSuiteSyncState WHERE RecordType = 'employee'
GO 



