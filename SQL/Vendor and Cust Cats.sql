USE [SecondSyncState]
GO

if exists (select * from sys.tables where name = 'NSManualVendorCategories')
DROP TABLE [dbo].[NSManualVendorCategories]
GO

CREATE TABLE [dbo].[NSManualVendorCategories](
	[ExternalId] [varchar](50) NOT NULL,
	[Display] [varchar](50) NOT NULL
) ON [PRIMARY]
GO

INSERT NSManualVendorCategories (ExternalId, Display) VALUES ('GolfPro','Golf Pro') 
INSERT NSManualVendorCategories (ExternalId, Display) VALUES ('CT','Club Trader') 
INSERT NSManualVendorCategories (ExternalId, Display) VALUES ('PBS','PBS') 
INSERT NSManualVendorCategories (ExternalId, Display) VALUES ('None','None') 



if exists (select * from sys.views where name = 'VI_CP_VendorCategories')
DROP VIEW VI_CP_VendorCategories
GO

/*PP 20180731 Get all the VendorCategories not already uploaded*/
CREATE VIEW VI_CP_VendorCategories
AS

select CATEG_COD, DESCR 
from PO_VEND_CATEG_COD cat
left join NetsuiteSyncState sync 
	on cat.categ_cod = sync.LocalId and sync.RecordType = 'VendorCategory'
where sync.Id is null

union all 
select ExternalId, Display 
from NSManualVendorCategories cat
left join NetsuiteSyncState sync 
	on cat.ExternalId = sync.LocalId and sync.RecordType = 'VendorCategory'
where sync.Id is null

GO


if exists (select * from sys.views where name = 'VI_CP_CustomerCategories')
DROP VIEW VI_CP_CustomerCategories
GO

/*PP 20180731 Get all the VendorCategories not already uploaded*/
CREATE VIEW VI_CP_CustomerCategories
AS

select CATEG_COD, DESCR 
from AR_CATEG_COD cat
left join NetsuiteSyncState sync 
	on cat.categ_cod = sync.LocalId and sync.RecordType = 'VendorCategory'
where sync.Id is null

GO

select * from VI_CP_CustomerCategories
select * from VI_CP_VendorCategories




