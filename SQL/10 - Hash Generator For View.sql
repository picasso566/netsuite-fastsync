﻿use secondsyncstate
go

if exists (select * from sys.objects where name = 'spSetFieldsForViewHashGenerator')
	drop procedure spSetFieldsForViewHashGenerator
go

if exists (select * from sys.objects where name = 'spGetDatatypeOfColumn')
	drop procedure spGetDatatypeOfColumn
go

create procedure spGetDatatypeOfColumn(
	@schema_name varchar(50), 
	@table_name as nvarchar(50), 
	@column_name varchar(50), 
	@columnDataType varchar(50) output)	
as
begin

	declare @sql nvarchar(4000),
			@ParmDefinition nvarchar(500);

	SET @ParmDefinition = N'@retval varchar(50) OUTPUT';

	set @sql = 'select @retval = data_type from ' + @schema_name + '.INFORMATION_SCHEMA.COLUMNS isc 
					where table_name = ''' + @table_name + ''' and column_name = ''' + @column_name + ''''
	exec sp_executesql @sql, @ParmDefinition, @retval = @columnDataType output;
	
end
go

create procedure spSetFieldsForViewHashGenerator
	@tableName varchar(200),
	@schema varchar(200),
	@HashOut varchar(max) OUTPUT,
	@print bit = 0,
	@newlineBetween bit = 0,
	@tableAlias varchar(50) = 'tbl',
	@excludeList varchar(4000) = '',
	@exclusiveList varchar(4000) = ''
	
as
begin
set nocount on
	declare @sql varchar(max),
			@hashSQL varchar(max),
			@debugText varchar(max),
			@columnName varchar(200), 
			@nullable varchar(3), 
			@datatype varchar(50), 
			@maxlen int,
			@newline varchar(20)

	if @newlineBetween = 1
		set @newline = CHAR(13)+CHAR(10)
	else
		set @newline = ''

	IF OBJECT_ID('tempdb.dbo.#schemaColumns') IS NOT NULL
	BEGIN
	  DROP TABLE #schemaColumns;
	END

	set nocount on

	create table  #schemaColumns  (COLUMN_NAME varchar(200), IS_NULLABLE varchar(5), DATA_TYPE varchar(50), CHARACTER_MAXIMUM_LENGTH int, ORDINAL_POSITION int)
	SET @sql = '
		INSERT #schemaColumns (COLUMN_NAME, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, ORDINAL_POSITION)
		SELECT isc.COLUMN_NAME, isc.IS_NULLABLE, isc.DATA_TYPE, isnull(isc.CHARACTER_MAXIMUM_LENGTH,''-1'') as CHARACTER_MAXIMUM_LENGTH, ORDINAL_POSITION
		from ' + @schema + '.INFORMATION_SCHEMA.COLUMNS isc
			WHERE isc.TABLE_NAME = ''' + @tableName + ''' '
			
		IF (LEN(@excludeList) > 0)			 
			SET @sql = @sql + ' AND isc.COLUMN_NAME NOT IN  (' + @excludeList + ') '
		IF (LEN(@exclusiveList) > 0)			 
			SET @sql = @sql + ' AND isc.COLUMN_NAME IN  (' + @exclusiveList + ') '

		SET @sql = @sql + '	ORDER BY ORDINAL_POSITION '

	EXEC (@sql)

	if(select count(*) from #schemaColumns ) = 0
	begin
		print 'No columns found for table / view ' + @schema + ' ' + @tableName
		print @sql
	end

	SET @debugText = ''
	SET @hashSQL = ' dbo.fnHashString( '

	DECLARE column_cursor CURSOR FOR 
	SELECT COLUMN_NAME, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH from #schemaColumns
	order by ORDINAL_POSITION

    OPEN column_cursor
    FETCH NEXT FROM column_cursor INTO @columnName, @nullable, @datatype, @maxlen

    WHILE @@FETCH_STATUS = 0
    BEGIN
			SELECT @debugText = @debugText + CHAR(13)+CHAR(10) + @tableAlias + '.[' + @columnName + '] | ' +  @datatype
			SELECT @hashSQL = @hashSQL + 

			CASE WHEN ( @datatype = 'decimal' or @datatype = 'money' or @datatype = 'int' or @datatype = 'tinyint' ) THEN
				 @newline + ' CASE WHEN ' + @tableAlias + '.[' + @columnName + '] IS NULL THEN '''' ELSE CONVERT( VARCHAR(20), ' + @tableAlias + '.[' + @columnName + '] ) END +'
			ELSE
				CASE WHEN @datatype = 'bit' THEN
					@newline + ' CASE WHEN ' + @tableAlias + '.[' + @columnName + '] IS NULL OR ' + @tableAlias + '.[' + @columnName + '] = 0 THEN ''N'' ELSE ''Y'' END +'
				ELSE
					CASE WHEN @datatype = 'uniqueidentifier' THEN
						 @newline + ' CASE WHEN ' + @tableAlias + '.[' + @columnName + '] IS NULL THEN '''' ELSE CONVERT( VARCHAR(50) , ' + @tableAlias + '.[' + @columnName + '] ) END +'
					ELSE
						CASE WHEN @datatype = 'datetime' or @datatype = 'smalldatetime'  or @datatype = 'date' THEN
							@newline + ' CASE WHEN ' + @tableAlias + '.[' + @columnName + '] IS NULL THEN '''' ELSE CONVERT( VARCHAR(20), ' + @tableAlias + '.[' + @columnName + '] ) END +'
						ELSE 
							CASE WHEN @datatype = 'timestamp' THEN
								@newline + ' CONVERT(NVARCHAR(MAX), CONVERT(BINARY(8), ' + @tableAlias + '.[' + @columnName + ']), 1) + '
							ELSE
								 --all strings:	
								 @newline + ' ISNULL(' + @tableAlias + '.[' + @columnName + '], '''') +'
							END
						END	
					END	
				END	
			END
		
		FETCH NEXT FROM column_cursor INTO @columnName, @nullable, @datatype, @maxlen
	END
    CLOSE column_cursor
    DEALLOCATE column_cursor

	--Take last plus off hashSQL
	if (@print = 1)
		print LEFT(@hashSQL, LEN(@hashSQL) - 1) + @newline +  ' )'
	
	SET @HashOut = LEFT(@hashSQL, LEN(@hashSQL) - 1) + @newline + ' )'
end
go



--Generates only the hashes for the table/view (for updating all the hashes)
if exists (select * from sys.objects where [name] = 'spGenerateAllHashesForTable')
	drop procedure spGenerateAllHashesForTable
go

create procedure spGenerateAllHashesForTable
	@tableName varchar(200),
	@schema varchar(200),
	@keyField varchar(200),
	@recordType varchar(200),
	@batchName varchar(200),
	@syncStateUpdate bit = 1,
	@useDistinct bit = 0
	
as
begin
set nocount on
	declare @finalSQL varchar(max),
			@debugText varchar(max),
    		@keyfieldType varchar(50),
			@keyfieldQuery varchar(1000),	
			@keyfieldCompare varchar(1000)

	exec spGetDatatypeOfColumn @schema,@tableName,@keyField, @keyfieldType output
	if(@keyfieldType = 'varchar' or @keyfieldType = 'char' or @keyfieldType = 'nvarchar' or @keyfieldType = 'character')
		SET @keyfieldQuery = 'tbl.' + @keyField + ' as LocalId, '
	else
		SET @keyfieldQuery = 'CAST(tbl.' + @keyField + ' as varchar(250)) as LocalId, '

	IF OBJECT_ID('tempdb.dbo.#schemaColumns') IS NOT NULL
	BEGIN
	  DROP TABLE #schemaColumns;
	END

	set nocount on

	SET @debugText = ''
	SET @finalSQL = ''

	declare @hash varchar(max)
	exec spSetFieldsForViewHashGenerator @tableName, @schema, @hash output, 0

	SET @finalSQL = 
	'/**************************** ' + @tableName + ' ****************************/' +
	case when @syncStateUpdate = 1 then
		CHAR(13)+CHAR(10) + 'IF OBJECT_ID(''tempdb..#newhashes'') IS NOT NULL ' +
		CHAR(13)+CHAR(10) + 'drop table #newhashes ' +
		CHAR(13)+CHAR(10) + 'go ' +
		CHAR(13)+CHAR(10) + 'CREATE TABLE #newhashes (LocalId Varchar(250),CurrentHash Varchar(32)) ' +
		CHAR(13)+CHAR(10) + 'insert #newhashes (LocalId, CurrentHash) ' 
	else 
		'' 
	end + 
	CHAR(13)+CHAR(10) + 'SELECT ' +
	CHAR(13)+CHAR(10) + @keyfieldQuery + 
	CHAR(13)+CHAR(10) + @hash +
	CHAR(13)+CHAR(10) + ' as NewHash ' +
	CHAR(13)+CHAR(10) + 'from ' + @schema + '.dbo.' + @tableName + ' tbl ' +

	case when @syncStateUpdate = 1 then
		CHAR(13)+CHAR(10) + ' ' +
		CHAR(13)+CHAR(10) + 'update NetSuiteSyncstate set LocalHash = h.CurrentHash ' +
		CHAR(13)+CHAR(10) + 'from NetSuiteSyncstate sync ' +
		CHAR(13)+CHAR(10) + 'join #newhashes h on sync.localId = h.LocalId ' +
		CHAR(13)+CHAR(10) + 'where sync.RecordType = ''' + @recordType + ''' and sync.nsBatchName = ''' + @batchName + '''' 
	else '' end

	print @finalSQL
end
go






if exists (select * from sys.objects where name = 'spGenerateNetSuiteGetChangedView')
	drop procedure spGenerateNetSuiteGetChangedView
go

create procedure spGenerateNetSuiteGetChangedView
	@tableName varchar(200),
	@schema varchar(200),
	@keyField varchar(200),
	@recordType varchar(200),
	@viewname varchar(200),
	@useDistinct bit = 0,
	@batchName varchar(200),
	@addToWhereClause varchar(4000) = '',
	@excludeList varchar(4000) = '',
	@exclusiveList varchar(4000) = ''
as
begin
set nocount on
	declare @finalSQL varchar(max),
			@debugText varchar(max),
			@keyfieldType varchar(50),
			@keyfieldQuery varchar(1000),	
			@keyfieldCompare varchar(1000)

	exec spGetDatatypeOfColumn @schema,@tableName,@keyField, @keyfieldType output
	if(@keyfieldType = 'varchar' or @keyfieldType = 'char' or @keyfieldType = 'nvarchar' or @keyfieldType = 'character')
	begin
		SET @keyfieldQuery = 'tbl.' + @keyField + ' as LocalId, '
		SET @keyfieldCompare = '		and sync.LocalId = tbl.' + @keyField + ' ' 
	end
	else
	begin
		SET @keyfieldQuery = 'CAST(tbl.' + @keyField + ' as varchar(250)) as LocalId, '
		SET @keyfieldCompare = '		and sync.LocalId = CAST(tbl.' + @keyField + ' as varchar(250)) ' 
	end


	IF OBJECT_ID('tempdb.dbo.#schemaColumns') IS NOT NULL
	BEGIN
	  DROP TABLE #schemaColumns;
	END

	set nocount on

	SET @debugText = ''
	SET @finalSQL =  'USE SecondSyncState ' + 
			CHAR(13)+CHAR(10) + 'go '+ CHAR(13)+CHAR(10) +
			CHAR(13)+CHAR(10) + 'if exists  (select * from sys.objects where name = ''' + @viewname + ''') ' +
			CHAR(13)+CHAR(10) + 'drop view ' + @viewname + ' ' +
			CHAR(13)+CHAR(10) + 'go '+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) +
			CHAR(13)+CHAR(10) + 'CREATE view ' + @viewname + ' ' +
			CHAR(13)+CHAR(10) + 'AS' +
			CHAR(13)+CHAR(10) + 'SELECT ' +	CASE WHEN @useDistinct = 1 THEN 'DISTINCT ' ELSE '' END +
			CHAR(13)+CHAR(10) + @keyfieldQuery
	print @finalSQL

	declare @hash varchar(max)
	exec spSetFieldsForViewHashGenerator @tableName, @schema, @hash output, 0

	print 'LocalHash =' +  @hash 
		
	print	CHAR(13)+CHAR(10) + ' from ' + @schema + '.dbo.' + @tableName + ' tbl ' +
			CHAR(13)+CHAR(10) + ' left join  NetSuiteSyncState sync on  ' +
			CHAR(13)+CHAR(10) + case when LEN(@batchName) > 0 then  '		sync.NSBatchName = ''' + @batchName + ''' and ' else '' end +
			CHAR(13)+CHAR(10) + '		sync.RecordType = ''' + @recordType + ''' ' +
			CHAR(13)+CHAR(10) + @keyfieldCompare +
			CHAR(13)+CHAR(10) + '		and sync.LocalHash = ' + @hash +
			CHAR(13)+CHAR(10) + ' where sync.Id is null' +

			case when LEN(@addToWhereClause) > 0 then CHAR(13)+CHAR(10) + @addToWhereClause else '' end + 
			CHAR(13)+CHAR(10) + ' GO' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10) 
end
go



if exists (select * from sys.objects where name = 'spGenerateNetSuiteGetChangesForJoinedTables')
	drop procedure spGenerateNetSuiteGetChangesForJoinedTables
go

create procedure spGenerateNetSuiteGetChangesForJoinedTables
	@tableName varchar(200),
	@schema varchar(200),
	@keyField varchar(200),
	@subqueryOrderBy varchar(200),
	@recordType varchar(200),
	@procname varchar(200),
	@batchName varchar(200),
	@addToWhereClause varchar(4000) = '',
	@addToWhereSubQuery varchar(4000) = ''
as
begin
set nocount on
	declare @finalSQL varchar(max),
			@debugText varchar(max),
			@keyfieldType varchar(50),
			@keyfieldQuery varchar(1000),	
			@keyfieldCompare varchar(1000),
			@hash varchar(max)

	exec spSetFieldsForViewHashGenerator @tableName, @schema, @hash output, 0, 0, 'subtbl'
	exec spGetDatatypeOfColumn @schema,@tableName,@keyField, @keyfieldType output

	if(@keyfieldType = 'varchar' or @keyfieldType = 'char' or @keyfieldType = 'nvarchar' or @keyfieldType = 'character')
	begin
		SET @keyfieldQuery = 'tbl.' + @keyField + ' as LocalId, '
		SET @keyfieldCompare = '		and sync.LocalId = tbl.' + @keyField + ' ' 
	end
	else
	begin
		SET @keyfieldQuery = 'CAST(tbl.' + @keyField + ' as varchar(250)) as LocalId, '
		SET @keyfieldCompare = '		and sync.LocalId = CAST(tbl.' + @keyField + ' as varchar(250)) ' 
	end


	IF OBJECT_ID('tempdb.dbo.#schemaColumns') IS NOT NULL
	BEGIN
	  DROP TABLE #schemaColumns;
	END

	set nocount on
	

	SET @debugText = ''
print  'USE SecondSyncState ' + 
			CHAR(13)+CHAR(10) + 'go '+ CHAR(13)+CHAR(10) +
			CHAR(13)+CHAR(10) + 'if exists  (select * from sys.procedures where name = ''' + @procname + ''') ' +
			CHAR(13)+CHAR(10) + 'drop procedure ' + @procname + ' ' +
			CHAR(13)+CHAR(10) + 'go '+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) +
			CHAR(13)+CHAR(10) + 'CREATE PROCEDURE ' + @procname + ' ' +
			CHAR(13)+CHAR(10) + '(@SqlQueryLimit int = 0)' +
			CHAR(13)+CHAR(10) + 'AS' +
			CHAR(13)+CHAR(10) + 'SET NOCOUNT ON;' +
			CHAR(13)+CHAR(10) + 'if @SqlQueryLimit > 0 ' +
			CHAR(13)+CHAR(10) + '  SET ROWCOUNT @SqlQueryLimit ' +
			CHAR(13)+CHAR(10) + 'SELECT tbl.LocalId as LocalId, tbl.LocalHash '
	
print 		CHAR(13)+CHAR(10) + 'from (	select maintbl.' + @keyField + ' as LocalId, ' +
			CHAR(13)+CHAR(10) + '	dbo.fnHashString(( ' 

print		CHAR(13)+CHAR(10) + '			select ' + @hash + ' ' 

print		CHAR(13)+CHAR(10) + '			from ' + @schema + '.dbo.' + @tableName + ' subtbl WHERE (subtbl.PoNo = maintbl.PoNo)  ' +
			CHAR(13)+CHAR(10) + '			order by subtbl.' + @subqueryOrderBy + ' ' +
			CHAR(13)+CHAR(10) + '			FOR XML PATH(''''),TYPE ).value(''(./text())[1]'',''VARCHAR(MAX)'') ' +
			CHAR(13)+CHAR(10) + '	)  as LocalHash ' +
			CHAR(13)+CHAR(10) + 'from ' + @schema + '.dbo.' + @tableName + ' maintbl  ' + @addToWhereSubQuery + 
			CHAR(13)+CHAR(10) + 'group by maintbl.' + @keyField + ' ) as tbl ' +
			CHAR(13)+CHAR(10) + ' left join  NetSuiteSyncState sync on  ' +
			CHAR(13)+CHAR(10) + case when LEN(@batchName) > 0 then  '  sync.NSBatchName = ''' + @batchName + ''' and ' else '' end +
			CHAR(13)+CHAR(10) + '		sync.RecordType = ''' + @recordType + ''' ' +
			CHAR(13)+CHAR(10) + '       and sync.LocalId = tbl.LocalId ' +
			CHAR(13)+CHAR(10) + '		and sync.LocalHash = tbl.LocalHash ' +
			CHAR(13)+CHAR(10) + '		and sync.NSSendSuccess = 1 ' +
			CHAR(13)+CHAR(10) + ' where sync.Id is null' +

			case when LEN(@addToWhereClause) > 0 then CHAR(13)+CHAR(10) + @addToWhereClause else '' end + 
			CHAR(13)+CHAR(10) + 'SET ROWCOUNT 0 ' +
			CHAR(13)+CHAR(10) + ' GO' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10) 


end
go








if exists (select * from sys.objects where name = 'spGenerateNetSuiteGetChangedQuery')
	drop procedure spGenerateNetSuiteGetChangedQuery
go

create procedure spGenerateNetSuiteGetChangedQuery
	@tableName varchar(200),
	@schema varchar(200),
	@keyField varchar(200),
	@recordType varchar(200),
	@procname varchar(200),
	@useDistinct bit = 0,
	@batchName varchar(200),
	@addToWhereClause varchar(4000) = '',
	@excludeList varchar(4000) = '',
	@exclusiveList varchar(4000) = ''
as
begin
set nocount on
	declare @finalSQL varchar(max),
			@debugText varchar(max),
			@keyfieldType varchar(50),
			@keyfieldQuery varchar(1000),	
			@keyfieldCompare varchar(1000)

	exec spGetDatatypeOfColumn @schema,@tableName,@keyField, @keyfieldType output
	if(@keyfieldType = 'varchar' or @keyfieldType = 'char' or @keyfieldType = 'nvarchar' or @keyfieldType = 'character')
	begin
		SET @keyfieldQuery = 'tbl.' + @keyField + ' as LocalId, '
		SET @keyfieldCompare = '		and sync.LocalId = tbl.' + @keyField + ' ' 
	end
	else
	begin
		SET @keyfieldQuery = 'CAST(tbl.' + @keyField + ' as varchar(250)) as LocalId, '
		SET @keyfieldCompare = '		and sync.LocalId = CAST(tbl.' + @keyField + ' as varchar(250)) ' 
	end


	IF OBJECT_ID('tempdb.dbo.#schemaColumns') IS NOT NULL
	BEGIN
	  DROP TABLE #schemaColumns;
	END

	set nocount on

	SET @debugText = ''
	SET @finalSQL =  'USE SecondSyncState ' + 
			CHAR(13)+CHAR(10) + 'go '+ CHAR(13)+CHAR(10) +
			CHAR(13)+CHAR(10) + 'if exists  (select * from sys.procedures where name = ''' + @procname + ''') ' +
			CHAR(13)+CHAR(10) + 'drop procedure ' + @procname + ' ' +
			CHAR(13)+CHAR(10) + 'go '+ CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10) +
			CHAR(13)+CHAR(10) + 'CREATE PROCEDURE ' + @procname + ' ' +
			CHAR(13)+CHAR(10) + 'AS' +
			CHAR(13)+CHAR(10) + 'SET NOCOUNT ON;' +
			CHAR(13)+CHAR(10) + 'SELECT ' +	CASE WHEN @useDistinct = 1 THEN 'DISTINCT ' ELSE '' END +
			
			CHAR(13)+CHAR(10) + @keyfieldQuery
	print @finalSQL

	declare @hash varchar(max)
	exec spSetFieldsForViewHashGenerator @tableName, @schema, @hash output, 0

	print 'LocalHash =' +  @hash 
		
	print	CHAR(13)+CHAR(10) + ' from ' + @schema + '.dbo.' + @tableName + ' tbl ' +
			CHAR(13)+CHAR(10) + ' left join  NetSuiteSyncState sync on  ' +
			CHAR(13)+CHAR(10) + case when LEN(@batchName) > 0 then  '		sync.NSBatchName = ''' + @batchName + ''' and ' else '' end +
			CHAR(13)+CHAR(10) + '		sync.RecordType = ''' + @recordType + ''' ' +
			CHAR(13)+CHAR(10) + @keyfieldCompare +
			CHAR(13)+CHAR(10) + '		and sync.LocalHash = ' + @hash +
			CHAR(13)+CHAR(10) + '		and sync.NSSendSuccess = 1 ' +
			CHAR(13)+CHAR(10) + ' where sync.Id is null' +

			case when LEN(@addToWhereClause) > 0 then CHAR(13)+CHAR(10) + @addToWhereClause else '' end + 
			CHAR(13)+CHAR(10) + 'SET ROWCOUNT 0 ' +
			CHAR(13)+CHAR(10) + ' GO' + CHAR(13)+CHAR(10) + CHAR(13)+CHAR(10)  + CHAR(13)+CHAR(10) 
end
go








--GENERATE JUST THE HASHES
--exec spGenerateAllHashesForTable 'NetSuite_VI_Vendor', 'secondsyncstate', 'vend_no', 'vendor','vendors', 1
--exec spGenerateAllHashesForTable 'NetSuite_VI_Customer', 'secondsyncstate', 'CustomerId', 'customer','cp_customers', 1
--exec spGenerateAllHashesForTable 'VI_CP_Users', 'secondsyncstate', 'USR_ID', 'employee','employees', 1
--exec spGenerateAllHashesForTable 'NetSuite_VI_ParentItem', 'secondsyncstate', 'SKU', 'inventoryItem', 'ParentItems', 1
--exec spGenerateAllHashesForTable 'NetSuite_VI_ItemUsed', 'secondsyncstate', 'CpItemNo', 'inventoryItem','UsedItems', 1
--exec spGenerateAllHashesForTable 'NetSuite_VI_ItemNew', 'secondsyncstate', 'CpItemNo', 'inventoryItem', 'NewItems', 1

declare @addWhere varchar(max)


----Vendors
--exec spGenerateNetSuiteGetChangedQuery 'NetSuite_VI_Vendor', 'SecondSyncState', 'vend_no', 'vendor', 'NetSuiteGetChangedVendorsFromCpGEN', 0, 'vendors'

----Customers:	
--exec spGenerateNetSuiteGetChangedQuery 'NetSuite_VI_Customer', 'secondsyncstate', 'CustomerId', 'customer', 'NetSuiteGetChangedCustomersFromCpGEN', 0, 'cp_customers'

----Employees
--exec spGenerateNetSuiteGetChangedQuery 'VI_CP_Users', 'SecondSyncState', 'USR_ID', 'employee', 'NetSuiteGetChangedEmployeesGEN', 0, 'employees'

----Parent Items
--exec spGenerateNetSuiteGetChangedQuery 'NetSuite_VI_ParentItem', 'SecondSyncState', 'SKU', 'inventoryItem', 'NetSuiteGetChangedParentItemsGEN', 0, 'ParentItems'

----Used Items:
--SET @addWhere = 'and tbl.itemid in ( SELECT distinct ItemId FROM ItemChangeLog_New Where TrxDateTime > DATEADD(day, -7, GETDATE()) )'
--exec spGenerateNetSuiteGetChangedQuery 'NetSuite_VI_ItemUsed', 'SecondSyncState', 'CpItemNo', 
--			'inventoryItem', 'NetSuiteGetChangedUsedItemsGEN', 0, 'UsedItems', @addWhere

----New Items:
--SET @addWhere = 'and tbl.itemid in ( SELECT distinct ItemId FROM ItemChangeLog_New Where TrxDateTime > DATEADD(day, -7, GETDATE()) )'
--exec spGenerateNetSuiteGetChangedQuery 'NetSuite_VI_ItemNew', 'SecondSyncState', 'CpItemNo', 
--			'inventoryItem', 'NetSuiteGetChangedNewItemsGEN', 0, 'NewItems', @addWhere


--GSV POs:
exec spGenerateNetSuiteGetChangesForJoinedTables 'NetSuite_VI_GSVPOHeaderLinesJoined', 'SecondSyncState', 'PoNo', 'Line_PoLineSeqNo',
				 'purchaseOrder', 'NetSuiteGetChangedGSVPOsGEN', 'GSVPurchaseOrder'

