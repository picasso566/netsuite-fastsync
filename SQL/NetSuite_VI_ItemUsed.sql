USE [SecondSyncState]
GO

/*
08/01/18 BF Create for Used Item QuickSync
20180805 PP Added formatted loft and length fields
20180809 PP Added join to condition table to get the Netsuite InternalId
*/
alter view [dbo].[NetSuite_VI_ItemUsed]
as
	select TIO.NsParentItemSku ,
			Cat.GsProductGroupCode,

			I.ItemId,
			I.TemplateItemNo,
			I.CpItemNo,
			I.UPC,
			I.Used,
			I.ItemTypeId,
			I.CategoryId,
			I.SubCategoryId,
			I.[Description],
			I.LongDescription,
			I.StockDescription,
			I.OnlineTitle,
			I.BounceId,
			I.BrandId,
			I.NumberOfClubsInIronSet,
			cond.AttrId as ConditionId,
			I.DexterityId,
			I.FreeNotes,
			I.[Length],
			I.LieAngleId,
			I.Loft,
			I.LocationId,
			I.ModelId,
			I.ShaftFlexId,
			I.ShaftTypeId,
			I.SellPrice,
			I.BidPrice,
			I.PricePremiumAmt,
			I.PricePremiumPct,
			I.PricePremiumApplyDate,
			I.PriceDiscountAmt,
			I.PriceDiscountPct,
			I.PriceDiscountApplyDate,
			I.UserId,
			I.DateCreated,
			I.MfrPriceRuleCode,
			I.OkForSf,
			I.OkForAz,
			I.OkForEb,
			I.ItemGUID,
			I.Source,
			I.ShaftMatl,
			I.TemplateId,
			I.Price,
			I.Cost,
			I.YearRelease,
			I.OkForStores,
			I.ClubDesiredBallFlight,
			I.Gripped,
			I.Torque,
			I.[Weight],
			I.Spin,
			I.Launch,
			I.ShaftClubType,
			I.Color,
			I.BagSize,
			I.Size,
			I.Gender,
			I.Fit,
			I.Style,
			I.StyleNo,
			I.Material,
			I.Pattern,
			I.NewLength,
			I.Width,
			I.Spikes,
			I.ShaftFlex,
			I.Bounce,
			I.ShaftType,
			I.Dexterity,
			I.LieAngle,
			I.ClubNo,
			I.RibbedOrRound,
			I.NumberOfGolfBalls,
			I.Category,
			I.Brand,
			I.Model,
			I.Condition,
			I.[Location],
			I.SubCategory,
			I.IsEcommerceItem,
			I.EbayTitle,
			I.ClearancePrice,
			I.BuyType,
			I.OpportunityBuy,
			I.OutOfStateSalePrice,
			I.MSRP,
			I.ChangedBy,
			I.DateChanged,
			I.ListOn2SOnly,
			I.Grind,
			I.HasImage

		   ,case when i.[Length] is not null and i.[Length] > 0 and  validLengths.ShaftLength is null then 'Invalid Length' else validLengths.FormattedLength end as FormattedLength
		   ,case when  validLofts.loft is not null then validLofts.loftformatted else '' end as FormattedLoft
    from   Item I	      
           inner join ViNsSkuFromTemplateItemNo TIO on TIO.TemplateItemNo = I.TemplateItemNo
		   inner join Category Cat on Cat.CategoryId = I.CategoryId
		   left join dbo.fnValidClubShaftLengths() validLengths on cast(validLengths.ShaftLength as varchar(10)) = isnull(i.[Length],'0')
		   left join dbo.fnValidLoftValues() validLofts on cast(validLofts.loft as varchar(10)) = isnull(i.Loft,'0')
		   left outer join dbo.Condition as cond on cond.ConditionId = I.ConditionId
    where  I.Used = 'Y';
go

