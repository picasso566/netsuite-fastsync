﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace netsuite_fastsync
{
    public static class NSExtMethods
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////   Custom Field Extension Methods ///////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void SetGSFieldByName(object o, NetSuiteHelper helper, string attributeName, string productGroup,
                NetsuiteCustomRecordDictionaryList netsuiteLists, List<CustomFieldRef> list)
        {
            var customRecordScriptId = "customrecord_g2_" + productGroup.ToLower() + "_" + attributeName.ToLower();
            var refFieldName = "custitem_g2_" + productGroup.ToLower() + "_" + attributeName.ToLower() + "_ref";
            var type = helper.CustomRecordRefForScriptId(customRecordScriptId);
            if (!netsuiteLists.ContainsKey(customRecordScriptId)) return;

            if (o.GetType().GetProperty(attributeName) == null) return;
            //Get the value of the property as a string
            string val = string.Empty;
            var res = o.GetType().GetProperty(attributeName).GetValue(o);
            if (res != null)
                val = res.ToString().ToLower();

            if (!netsuiteLists[customRecordScriptId].ContainsKey(val)) return;
            list.Add(new SelectCustomFieldRef()
            {
                scriptId = refFieldName,
                value = new ListOrRecordRef()
                {
                    typeId = type.internalId,
                    internalId = netsuiteLists[customRecordScriptId][val]
                }
            });
        }

        public static void CustomItemFieldLookupValueFromList(this int? Value, NetSuiteHelper helper, string typeName, string custFieldName, NetsuiteCustomRecordDictionaryList netsuiteLists, List<CustomFieldRef> list)
        {
            if (Value == null || Value == 0) return;
            (Value ?? 0).ToString().CustomItemFieldLookupValueFromList(helper, typeName, custFieldName, netsuiteLists, list);
        }


        public static void CustomItemFieldLookupValueFromList(this string Value, NetSuiteHelper helper, string typeName, string custFieldName, NetsuiteCustomRecordDictionaryList netsuiteLists, List<CustomFieldRef> list)
        {
            if (!netsuiteLists.ContainsKey(typeName)) return;
            var lookupTable = netsuiteLists[typeName];

            if (Value == null) return;
            Value = Value.ToLower();
            if (!lookupTable.ContainsKey(Value)) return;

            var lookupVal = lookupTable[Value];

            var type = helper.CustomRecordRefForScriptId(typeName);
            list.Add(new SelectCustomFieldRef()
            {
                scriptId = custFieldName,
                value = new ListOrRecordRef()
                {
                    typeId = type.internalId,
                    internalId = lookupVal
                }
            });
        }

        public static void AddItemPriceToPriceList(this decimal? Value, string priceLevelName, Dictionary<string, RecordRef> PriceLevels, List<Pricing> prices)
        {
            if (Value == null)
                return;
            decimal Val2 = (Value ?? 0);
            AddPriceToPriceList(Val2, priceLevelName, PriceLevels, prices);
        }

        public static void AddItemPriceToPriceList(this decimal Value, string priceLevelName, Dictionary<string, RecordRef> PriceLevels, List<Pricing> prices)
        {
            AddPriceToPriceList(Value, priceLevelName, PriceLevels, prices);
        }

        private static void AddPriceToPriceList(decimal Value, string priceLevelName, Dictionary<string, RecordRef> PriceLevels, List<Pricing> prices)
        {
            var val = Math.Abs(Value);
            val = Math.Round(val, 2);
            if (val == 0) return;
            var p = new Pricing()
            {
                priceLevel = PriceLevels[priceLevelName],
                priceList = new Price[]{
                                    new Price(){
                                        value = (double)val,
                                        valueSpecified = true,
                                        quantity = 0,
                                        quantitySpecified = true
                                    }
                                }
            };
            prices.Add(p);
        }


        public static void CustomItemFieldLookupValue(this int? Value, NetSuiteHelper helper, string typeName, string custFieldName, List<CustomFieldRef> list)
        {
            if (Value == null) return;
            var itemTypeExternalId = Value.ToString();
            if (itemTypeExternalId == "0") return;
            var itemTypeType = helper.CustomRecordRefForScriptId(typeName);
            list.Add(new SelectCustomFieldRef() { internalId = helper.CustomItemFieldRefForScriptId(custFieldName).internalId, value = new ListOrRecordRef() { typeId = itemTypeType.internalId, externalId = itemTypeExternalId } });
        }

        public static void CustomItemFieldLookupValue(this int Value, NetSuiteHelper helper, string typeName, string custFieldName, List<CustomFieldRef> list)
        {
            var itemTypeExternalId = Value.ToString();
            if (itemTypeExternalId == "0") return;
            var itemTypeType = helper.CustomRecordRefForScriptId(typeName);
            list.Add(new SelectCustomFieldRef() { internalId = helper.CustomItemFieldRefForScriptId(custFieldName).internalId, value = new ListOrRecordRef() { typeId = itemTypeType.internalId, externalId = itemTypeExternalId } });
        }

        public static void CustomItemFieldLookupValue(this string Value, NetSuiteHelper helper, string typeName, string custFieldName, List<CustomFieldRef> list)
        {
            if (Value.IsNullOrEmpty()) return;
            var itemTypeExternalId = Value;
            if (itemTypeExternalId == "0") return;
            var itemTypeType = helper.CustomRecordRefForScriptId(typeName);
            list.Add(new SelectCustomFieldRef() { internalId = helper.CustomItemFieldRefForScriptId(custFieldName).internalId, value = new ListOrRecordRef() { typeId = itemTypeType.internalId, externalId = itemTypeExternalId } });
        }

        public static void CustomRecordFieldLookupValue(this string Value, NetSuiteHelper helper, string typeName, string custFieldName, List<CustomFieldRef> list)
        {
            if (Value.IsNullOrEmpty()) return;
            var itemTypeExternalId = Value;
            if (itemTypeExternalId == "0") return;
            var itemTypeType = helper.CustomRecordRefForScriptId(typeName);
            list.Add(new SelectCustomFieldRef() { scriptId = custFieldName, value = new ListOrRecordRef() { typeId = itemTypeType.internalId, externalId = itemTypeExternalId } });
        }

        public static void CustomFieldStringValue(this string Value, string name, List<CustomFieldRef> list)
        {
            if (Value.IsNullOrEmpty()) return;
            list.Add(new StringCustomFieldRef() { scriptId = name, value = Value });
        }

        public static void CustomFieldDateValue(this DateTime? Value, string name, List<CustomFieldRef> list)
        {
            if (Value == null) return;
            list.Add(new DateCustomFieldRef() { scriptId = name, value = (Value ?? DateTime.Now) });
        }

        public static void CustomFieldDateValue(this DateTime Value, string name, List<CustomFieldRef> list)
        {
            list.Add(new DateCustomFieldRef() { scriptId = name, value = Value });
        }

        public static void CustomFieldDateValue(this string Value, string name, List<CustomFieldRef> list, ILog log)
        {
            if (Value.IsNullOrEmpty()) return;
            DateTime dateValue;
            try
            {
                dateValue = DateTime.Parse(Value);
                list.Add(new DateCustomFieldRef() { scriptId = name, value = dateValue });
            }
            catch (Exception ex)
            {
                log.Warn(Value + " is not a valid date time value, didn't set " + name, ex);
            }
        }

        public static void CustomFieldCurrencyValue(this string Value, string name, List<CustomFieldRef> list)
        {
            if (Value.IsNullOrEmpty()) return;
            double doubleVal;
            if (double.TryParse(Value, out doubleVal))
            {
                doubleVal = Math.Round(doubleVal, 2);
                list.Add(new DoubleCustomFieldRef() { scriptId = name, value = doubleVal });
            }
        }

        public static void CustomFieldCurrencyValue(this decimal? Value, string name, List<CustomFieldRef> list)
        {
            if (Value == null) return;
            double doubleVal;
            if (double.TryParse(Value.ToString(), out doubleVal))
            {
                doubleVal = Math.Round(doubleVal, 2);
                list.Add(new DoubleCustomFieldRef() { scriptId = name, value = doubleVal });
            }
        }

        public static void CustomFieldFloatValue(this int? Value, string name, List<CustomFieldRef> list)
        {
            if (Value == null) return;
            list.Add(new DoubleCustomFieldRef() { scriptId = name, value = (double)Value.GetValueOrDefault(0) });
        }


        public static void CustomFieldFloatValue(this decimal? Value, string name, List<CustomFieldRef> list)
        {
            if (Value == null) return;
            list.Add(new DoubleCustomFieldRef() { scriptId = name, value = (double)Value.GetValueOrDefault(0) });
        }

        public static void CustomFieldFloatValue(this string Value, string name, List<CustomFieldRef> list)
        {
            if (Value.IsNullOrEmpty()) return;
            double doubleVal;
            if (double.TryParse(Value, out doubleVal))
            {
                list.Add(new DoubleCustomFieldRef() { scriptId = name, value = doubleVal });
            }
        }

        public static void CustomFieldIntValue(this int? Value, string name, List<CustomFieldRef> list)
        {
            if (Value == null) return;
            CustomFieldIntValue((Value ?? 0), name, list);
        }

        public static void CustomFieldIntValue(this int Value, string name, List<CustomFieldRef> list)
        {
            list.Add(new LongCustomFieldRef() { scriptId = name, value = Value });
        }


        public static void CustomFieldIntValue(this string Value, string name, List<CustomFieldRef> list)
        {
            if (Value.IsNullOrEmpty()) return;
            int intVal;
            if (int.TryParse(Value, out intVal))
            {
                list.Add(new LongCustomFieldRef() { scriptId = name, value = intVal });
            }
        }

        public static void CustomFieldCheckboxValue(this bool Value, string name, List<CustomFieldRef> list)
        {
            list.Add(new BooleanCustomFieldRef() { scriptId = name, value = Value });
        }


        public static void CustomFieldKitServiceItemReference(this Guid? Value, string custFieldName, List<CustomFieldRef> list)
        {
            if (Value == null) return;
            CustomFieldKitServiceItemReference(Value ?? new Guid(), custFieldName, list);
        }

        public static void CustomFieldKitServiceItemReference(this Guid Value, string custFieldName, List<CustomFieldRef> list)
        {
            CustomFieldKitServiceItemReference(Value.ToString(), custFieldName, list);
        }

        public static void CustomFieldKitServiceItemReference(this string Value, string custFieldName, List<CustomFieldRef> list)
        {
            if (Value.IsNullOrEmpty()) return;
            list.Add(new SelectCustomFieldRef()
            {
                scriptId = custFieldName,
                value = new ListOrRecordRef()
                {
                    typeId = ((int)RecordType.serviceSaleItem).ToString(),
                    externalId = Value
                }
            });
        }


        public static RecordRef ExternalIdToRecordRef(this string externalId, string IdPrefix, RecordType recordType)
        {
            return new RecordRef() { externalId = IdPrefix + externalId, type = recordType, typeSpecified = true };
        }


        private static Regex videoIdPattern = new Regex(".*(\\.be|v|embed)[=/]([^&\"]+).*");

        public static ListOrRecordRef UrlToRef(string url)
        {
            var match = videoIdPattern.Match(url);
            return new ListOrRecordRef()
            {
                typeId = "customrecord_video",
                externalId = ((match.Success) ? match.Groups[2].Value : url.GetHashCode().ToString())
            };
        }

        public static List<ListOrRecordRef> VideoFieldsToListOfRefs(NetsuiteLookuper lookups, object itemRecord)
        {
            var vidRefs = new List<ListOrRecordRef>();
            foreach (var vidType in lookups.VIDEO_FIELD_PREFIXES)
            {
                for (var vidNum = 1; vidNum < 6; vidNum++)
                {
                    var fieldName = vidType + "VideoURL_0" + vidNum;
                    string videoUrl = (ExtMethods.PropertyExistsNotNull(itemRecord, fieldName)) ? ExtMethods.GetPropertyFromObject<string>(itemRecord, fieldName) : "";
                    if (videoUrl.Length > 0)
                        vidRefs.Add(UrlToRef(videoUrl));
                }
            }
            return vidRefs;
        }

    }
}
