﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace netsuite_fastsync
{
    public static class ExtMethods
    {
        /// <summary>
        /// Return a full path to a file. 
        /// Example: "something.csv".ToApplicationPath()  ->  c:\development\MyApp\bin\Debug\netcoreapp2.0\something.csv
        /// and "../../csvFiles/something.csv".ToApplicationPath()  ->  c:\development\MyApp\bin\csvFiles\something.csv
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string ToApplicationPath(this string fileName)
        {
            var exePath = Path.GetDirectoryName(System.Reflection
                                .Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;
            return Path.Combine(appRoot, fileName);
        }

        public static bool IsNullOrEmpty(this String value)
        {
            return (value == null || value.Length == 0);
        }

        public static async System.Threading.Tasks.Task ForEachAsync<T>(this List<T> list, Func<T, System.Threading.Tasks.Task> func)
        {
            foreach (var value in list)
            {
                await func(value);
            }
        }
       
        public static (string first, string last) ReconcileFirstLastNAMFromCP(this string NAM, string currentFirst, string currentLast)
        {
            var spaced = NAM.NetsuiteFirstLastNameBySpaces();
            string finalFirst = currentFirst, finalLast = currentLast;
            
            if (currentFirst.IsNullOrEmpty() && !spaced.first.IsNullOrEmpty())
                finalFirst = spaced.first;
            if(currentLast.IsNullOrEmpty() && !spaced.last.IsNullOrEmpty())
                finalLast = spaced.last;
            if (finalFirst.IsNullOrEmpty())
                finalFirst = string.Empty;
            if (finalLast.IsNullOrEmpty())
                finalLast = string.Empty;
            return (finalFirst, finalLast);
        }



        /// <summary>
        /// Try to split a name up by spaces. The first is the first and the last possible match is the lastname.
        /// Return the string as a firstname if there's no spaces
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static (string first, string last) NetsuiteFirstLastNameBySpaces(this string val)
        {
            var namParts = val.Split(' ');
            var firstName = "";
            var lastName = "";

            if (namParts.Length > 1)
            {
                firstName = namParts[0];
                lastName = namParts[namParts.Length - 1];
            }
            else
                firstName = val;

            return (firstName, lastName);
        }

        public static string MaximumLengthString(this string val, int maxLength) {
            if (val == null)
                return string.Empty;
            return val.Substring(0, Math.Min(val.Length, maxLength));
        }

        /// <summary>
        /// Format a string to be an appropriate external Id in Netsuite
        /// Basically strip spaces and non alpha numerics
        /// </summary>
        /// <returns></returns>
        public static string NetsuiteExtIdFormat(this string val)
        {
            val = val.Replace("/", "-");
            val = val.Replace(" ", "-");
            val = new Regex("[^a-zA-Z0-9-]").Replace(val, "");
            return val;
        }


        public static string ValueOrDefault(this String value, string defaultValue)
        {
            return (value.IsNullOrEmpty()) ? defaultValue : value;
        }


        public static string ValueOrDefaultTrimmedNullable(this String value, string defaultValue)
        {
            return (value.IsNullOrEmpty()) ? defaultValue : value.Trim();
        }


        public static Country GetCountry(this String value)
        {
            CountryMap cm = new CountryMap();
            var sCntry = value.ValueOrDefault("united states").ToLower().Trim();

            if (cm.ContainsKey(sCntry))
                return cm[sCntry];

            Country c;
            if (Enum.TryParse<Country>(sCntry.Replace(" ", ""), true, out c)) return c;
            if (Enum.TryParse<Country>("_" + sCntry.Replace(" ", ""), true, out c)) return c;
            throw new Exception("Could not turn " + sCntry + " into a country enum value.");
        }

        public static T GetPropertyFromObject<T>(object o, string property)
        {
            if (!PropertyExistsNotNull(o, property))
                return default(T);

            var value = o.GetType().GetProperty(property).GetValue(o);
            return (T)Convert.ChangeType(value, typeof(T));
        }

        public static bool PropertyExistsNotNull(object o, string property)
        {
            if ((o.GetType().GetProperty(property) == null))
                return false;
            if (o.GetType().GetProperty(property).GetValue(o) == null)
                return false;
            return true;
        }


        /// <summary>
        /// PP 20180714 - Conversion of the method from the Datamap to an extension method
        /// PP 20180426 - Pauls update to Jason's original email validator/corrector. This is plain old stupid (and a little crazy). 
        /// The only reasons for this code are:
        /// 1) Netsuite's email validation does not follow the email adress format RFCs
        /// 2) If an email does not fit Netsuite's version of acceptable, the entire vendor record is rejected.
        /// 3) The email hand entered and often has issues. We prefer to import as much as possible instead of removing it.
        /// 
        /// Jason's comment is below.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        // pretty ugly hack to do something to the emails netsuite is complaining about...
        // (.net is doing the right thing here, saying that xxx@yyy is valid, but netsuite seems to require a host.tld 
        // after the @... which 99.999% of emails are, but is technically not part of the spec...)
        // most instances I've found in the wild in our database just need a .com after them to be valid...
        public static string GetEmailForNetsuite(this string key)
        {
            var email = key.ValueOrDefault("");
            if (email == string.Empty)
                return null;

            try
            {
                //if there's no @, sorry, nothing can be done.
                if (!email.Contains("@"))
                    return null;

                //Get rid of dots right after the @ or double dots
        
                email = new Regex(@"[.]{2,}", RegexOptions.IgnoreCase).Replace(email, ".").Trim();
                email = new Regex(@"[@]{2,}", RegexOptions.IgnoreCase).Replace(email, "@").Trim();

                //This should never be done normally, but Netsuite does not seem to allow unicode. 
                //This breaks the RFC dealing with local addresses allowing comments and quoted identifiers
                //As well as foreign language email addresses. 
                //I am just striping out anything that can cause the record to fail. 
                //Some emails have this character in them: L
                email = Regex.Replace(Regex.Replace(email, @"[\\~#%&*{}/:,;\[\]<>?|\"" -]", ""), @"\s+", " ");
                email = email.Replace("L", "");

                email = email.Replace("@.", "@");

                var splitParts = email.Replace(" ", "").Split('@');
                var name = splitParts[0];
                var domain = splitParts[1];

                //If the domain does not have a dot, check if they forgot the dot: "jane@googlecom" 
                //or there isn't a top level at all: "jane@google"... assume .com as the top level. 
                var dotIdx = domain.IndexOf('.');
                if (dotIdx == -1)
                {
                    if (domain.Substring(domain.Length - 3) == "com")
                        domain = domain.Substring(0, domain.Length - 3) + ".com";
                    else
                        domain += ".com";
                }
                else
                {
                    //Netsuite will not accept single character top level domains
                    var domParts = domain.Split('.');
                    if (domParts[domParts.Length - 1].Length < 2)
                        return null;

                    //all other parts of the domain must be at least 1 character 
                    for (var x = 0; x < (domParts.Length - 1); x++)
                    {
                        if (domParts[x].Length == 0)
                            return null;
                    }

                    //The top level cannot be more than 16 characters
                    if (domParts[domParts.Length - 1].Length > 16)
                        return null;
                }

                //the name portion cannot begin or end with a dot:
                if (name.StartsWith("."))
                    name = name.Substring(1);
                if (name.EndsWith("."))
                    name = name.Substring(0, name.Length - 1);

                //reassemble:
                email = name + "@" + domain;

                //Check that the final email is a good format (according to Microsofts validation standards)
                if (!(new EmailAddressAttribute().IsValid(email)))
                    return null;

                return email;
            }
            catch
            {
                return null;
            }
        }


        public static string GetPhoneNumberOrDefault(this string phone, string defaultValue)
        {
            if (phone.IsNullOrEmpty()) return defaultValue;

            var finalPhone = new Regex(@"[^\d]").Replace(phone, string.Empty);

            if (finalPhone.Length == 10)
                return finalPhone.Substring(0, 3) + "-" + finalPhone.Substring(3, 3) + "-" + finalPhone.Substring(6);
            else if (finalPhone.Length == 6)
                return finalPhone.Substring(0, 3) + "-" + finalPhone.Substring(3);

            return defaultValue;
            //return finalPhone;
        }

    }
}
