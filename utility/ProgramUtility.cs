﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading;
using log4net.Core;
using System.Diagnostics;
using System.IO;
using _2ndswingCommon.services;
using System.Text;

namespace netsuite_fastsync
{
    class ProgramUtility
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        private static readonly ILog JrnlLog = LogManager.GetLogger(typeof(Program));

        public readonly HashSet<string> CommandsToRun = new HashSet<string>();
        public readonly Dictionary<string, List<string>> RunSets = new Dictionary<string, List<string>>() { };
        public DateTime StartDate = DateTime.Parse("2000-01-01");
        public DateTime EndDate = DateTime.Parse("2099-01-01");
        public int BatchSize = 20;

        public ProgramUtility()
        {
            ExtractCommandsFromConfig();
            StartDate = DateTime.Parse(Configuration.Get("SyncStartDate"));
            EndDate = DateTime.Parse(Configuration.Get("SyncEndDate"));
            BatchSize = int.Parse(Configuration.Get("BatchSize"));
        }

        public void ExtractCommandsFromConfig()
        {
            var list = Configuration.Get("CommandsToRun");
            var args = list.Split(',');
            foreach (var arg in args)
            {
                var arg2 = arg.Trim();
                if (arg2.Length <= 0) continue;
                if (RunSets.ContainsKey(arg2))
                    foreach (var cmd in RunSets[arg2])
                        CommandsToRun.Add(cmd);
                else
                    CommandsToRun.Add(arg2);
            }


            if (CommandsToRun.Count == 0)
            {
                Log.Info("No commands, so exiting with error code 1.");
                Console.ReadLine();
            }

            foreach (var cmd in CommandsToRun)
                Log.InfoFormat("\t{0}", cmd);

        }

        public bool ConditionalRun(string commandName)
        {
            if (CommandsToRun.Contains(commandName) || CommandsToRun.Count == 0)
            {
                Log.InfoFormat("++++ Running Command: {0}", commandName);
                return true;
            }
            else
                return false;
        }

        public IObserver<NetsuiteRecordWrapper> CreateBatchedUpsertObserver(string name, int size)
        {
            return CreateBatchedObserver(BatchActionType.upsert, name, size);
        }
        public IObserver<NetsuiteRecordWrapper> CreateBatchedObserver(BatchActionType actionType, string name, int size)
        {
            // NOTE below: we increase size * 4, because we're running 4 updates at a time now.
            var sendToNS = Configuration.Get("SendToNS");
            var logRecordXml = Boolean.Parse(Configuration.Get("LogRecordXml"));
            var batchMultiplier = Configuration.Get("BatchMultiplier");
            var batchMultInt = 1;
            if (batchMultiplier != null)
                batchMultInt = int.Parse(batchMultiplier);

            if (Boolean.Parse(sendToNS))
                return new BetterBatchedObserver(actionType, name, size * batchMultInt, Level.Info);
            else
            {
                if (logRecordXml)
                    return new LoggingRecordObserver();
                else
                    return new EmptyObserver();
            }
        }

        public NetsuiteRecordWrapper LogEmployeeQAWarnings(NetsuiteRecordWrapper w)
        {
            Employee r = ((Employee)w.NetsuiteRecord);
            if (r.firstName?.Length > 32)
                Log.WarnFormat("Employee '{0}' has firstname '{1}' which is > 32 characters.", r.externalId, r.firstName);
            if (r.initials?.Length > 3)
                Log.WarnFormat("Employee '{0}' has initials '{1}' which is > 32 characters.", r.externalId, r.initials);
            return w;
        }
    }
}
