﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;

namespace netsuite_fastsync
{
    public class NetsuiteLookuper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        NetSuiteHelper helper;
        public NetsuiteCustomRecordDictionaryList ItemAttibutes;
        public RecordRef CogsAccount;
        public RecordRef AssetAccount;
        public RecordRef shippingAccount;
        public RecordRef UsedClass;
        public RecordRef NewClass;
        public RecordRef OpportunityClass;
        public RecordRef ConditionRef;
        public Dictionary<string, RecordRef> ItemTypeIdToClass;
        public Dictionary<string, RecordRef> PriceLevels;

        public NetsuiteLookuper(NetSuiteHelper helper)
        {
            this.helper = helper;
        }

      
        public void PopulateItemLookups()
        {
            Log.InfoFormat("Retreiving item custom record lists from Netsuite.");
            ItemAttibutes = GetItemAttributeLookups();
            ItemAttibutes.MergeList(GetItemCodeLookups());
            Log.InfoFormat("{0} Item attribute lists retreived. {1} Values", ItemAttibutes.Keys.Count(), ItemAttibutes.TotalRecords);

            PopulateItemAccounts();
        }
        public void PopulatePOLookups() {

        }

        public void PopulatePOAccounts() {
            if (ConditionRef == null) { 
                ConditionRef = helper.CustomRecordRefForScriptId("customrecord_g2_condition");
            }
            if (ItemAttibutes == null)
                ItemAttibutes = new NetsuiteCustomRecordDictionaryList();
            if (!ItemAttibutes.ContainsKey("customrecord_g2_condition"))
                ItemAttibutes.Add("customrecord_g2_condition", GetItemCodeLookup("customrecord_g2_condition").dict);
            if (!ItemAttibutes.ContainsKey("customrecord_g2_shaft_flex"))
                ItemAttibutes.Add("customrecord_g2_shaft_flex", GetItemCodeLookup("customrecord_g2_shaft_flex").dict);

            if(CogsAccount == null)
                PopulateItemAccounts();
        }

        public string[] VIDEO_FIELD_PREFIXES = new string[] { "Stock", "Product", "TemplateStock", "TemplateProduct", "TemplateModelStock", "TemplateModelProduct" };
        public string[] DefaultIrons = new string[] { "4", "5", "6", "7", "8", "9", "PW" };

        public Dictionary<string, string[]> ItemAttributeNames = new Dictionary<string, string[]>() {
                    {"BagSize", new string[] {}},
                    {"DesiredBallFlight", new string[] {"club"} },
                    {"Color", new string[] {"accessory","bag","bottom","club","hat","shoes","top","gpsrange"}},
                    {"Fit", new string[] {"hat"}},
                    {"Gender", new string[] {"accessory","bottom","club","hat","headcover","shoes","top"}},
                    {"Grind", new string[] {"club"}},
                    {"HeadSize", new string[] {"club"}},
                    {"ManufacturingType", new string[] {"club"}},
                    {"SpinRate", new string[] {"club"}},
                    {"Launch", new string[] {"club"}},
                    {"Loft", new string[] {"club"}},
                    {"Length", new string[] {"bottom", "shaft"}},
                    {"Material", new string[] {"accessory","bottom","top"}},
                    {"Number", new string[] {"club"}},
                    {"Pattern", new string[] {"bottom","top"}},
                    {"PutterHeadStyle", new string[] {"club"}},
                    {"ShaftMatl", new string[] {"club"}},
                    {"Size", new string[] {"accessory", "bottom", "hat", "shoes", "top"}},
                    {"Spikes", new string[] {"shoes"}},
                    {"Spin", new string[] {"shaft"}},
                    {"Style", new string[] {"bottom","hat","top"}},
                    {"ToeHang", new string[] {"club"}},
                    {"Weight", new string[] {"shaft"}},
                    {"Width", new string[] {"shoes"}},
                  };

        List<string> ItemCodeNames = new List<string>() {
                {"customrecord_g2_shaft_type"},
                {"customrecord_g2_shaft_flex"},
                {"customrecord_g2_dexterity"},
                {"customrecord_g2_bounce"},
                {"customrecord_g2_condition"}
             };

       


        public NetsuiteCustomRecordDictionaryList GetItemCodeLookups()
        {
            NetsuiteCustomRecordDictionaryList list = new NetsuiteCustomRecordDictionaryList();
            foreach (var typename in ItemCodeNames)
            {
                var coderesult = GetItemCodeLookup(typename);
                list.Add(coderesult.name, coderesult.dict);
            }
            return list;
        }

        public (string name, NetsuiteCustomRecordDictionary dict) GetItemCodeLookup(string typename) {

            var dict = new NetsuiteCustomRecordDictionary(typename);
            var results = helper.FindAll(typename).ToList();
            foreach (var r in results)
            {
                string name = null;
                if (r.customFieldList == null)
                    name = r.name.ToLower();
                else
                {
                    foreach (var customFieldRef in r.customFieldList)
                        if (customFieldRef.scriptId.EndsWith("_code"))
                            name = ((StringCustomFieldRef)customFieldRef).value.ToLower();
                }
                dict.Add(name, r.internalId);
            }
            return (typename, dict);
        }


        public NetsuiteCustomRecordDictionaryList GetItemAttributeLookups()
        {
            NetsuiteCustomRecordDictionaryList list = new NetsuiteCustomRecordDictionaryList();

            foreach (var att in ItemAttributeNames)
            {
                foreach (var pgName in att.Value)
                {
                    var typename = "customrecord_g2_" + pgName.ToLower() + "_" + att.Key.ToLower();
                    var allRecs = helper.FindAll(typename);
                    list.AddToDictionary(typename, allRecs);
                }
            }
            return list;
        }


        public void PopulateItemAccounts()
        {
            CogsAccount = helper.RecordToRef(helper.FindOne<Account>("number", "startsWith", "5010"));
            AssetAccount = helper.RecordToRef(helper.FindOne<Account>("number", "startsWith", "1601"));
            UsedClass = helper.RecordToRef(helper.FindOne<Classification>("name", "is", "400 Used"));
            NewClass = helper.RecordToRef(helper.FindOne<Classification>("name", "is", "401 New"));
            OpportunityClass = helper.RecordToRef(helper.FindOne<Classification>("name", "is", "403 Opportunity"));
            PriceLevels = helper.FindAll<PriceLevel>().ToDictionary(rec => rec.name, rec => helper.RecordToRef(rec));
            shippingAccount = helper.RecordToRef(helper.FindOne<Account>("number", "startsWith", "5060"));

            Log.InfoFormat("Retreiving ItemTypeIdToClass.");
            ItemTypeIdToClass = helper.FindAll<Classification>()
                    .Where(r => { return r.externalId != null; })
                    .ToDictionary(
                        r => { return r.externalId; },
                        r => { return new RecordRef() { type = RecordType.classification, typeSpecified = true, internalId = r.internalId }; });

            Log.InfoFormat("{0} ItemTypeIdToClasses received", ItemTypeIdToClass.Keys.Count());
        }
    }
}
