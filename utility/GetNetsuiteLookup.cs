﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace netsuite_fastsync
{
   public  class GetNetsuiteLookup
    {

        NetSuiteHelper _helper;

        public GetNetsuiteLookup() {
            _helper = new NetSuiteHelper();
        }

        public List<string> ValidGSVSites()
        {
            var validGSVSites = new List<string>();
            CustomRecordSearch s = new CustomRecordSearch() { basic = new CustomRecordSearchBasic() { recType = _helper.CustomRecordRefForScriptId("customrecord_gsv_site") } };
            SearchPreferences prefs = new SearchPreferences() { bodyFieldsOnly = false, pageSize = 100, pageSizeSpecified = true, returnSearchColumns = true };
            var models = _helper.FindAll<CustomRecord>(s, prefs).ToList();
            foreach (var model in models)
                validGSVSites.Add(model.customFieldList.Where(r => r.scriptId == "custrecord_gsv_site_guid")
                        .Select(r => ((StringCustomFieldRef)r).value).FirstOrDefault()
                    );
            return validGSVSites;
        }

    }
}
