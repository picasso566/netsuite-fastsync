﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;

namespace netsuite_fastsync
{
    public enum ForwardSyncProcessNames
    {
        GSVPurchaseOrder,
        ParentItem,
        UsedItem,
        Vendor,
        Customer,
        Employee
    }

    /// <summary>
    /// This is plain dumb. I do not understand how the configuration is set up. It seems to have a wrapper
    /// so the normal built in functions for parsing them don't seem to work. I don't have time to investiagate.
    /// Wrote these quick and dirty (crappy) setting parsers.
    /// </summary>
    public static class ForwardSyncProcessSettings
    {
        public static Dictionary<string, ForwardSyncProcessSetting> GetProcessSettings() {

            Dictionary<string, ForwardSyncProcessSetting> finalSettings = new Dictionary<string, ForwardSyncProcessSetting>();

            var settings = Configuration.Config.GetSection("ForwardSyncProcessSetting")
                     .GetChildren();
            foreach (var x in settings)
            {
                var vals = new ForwardSyncProcessSetting();
                var c = x.GetChildren().ToDictionary(r => r.Key, r => r.Value);
                vals.SqlQueryLimit = Convert.ToInt32(c["SqlQueryLimit"]);
                finalSettings.Add(x.Key, vals);
            }
            return finalSettings;
        }

        public static ForwardSyncProcessSetting GetProcessSetting(ForwardSyncProcessNames SpecificProcess)
        {
            var setting = new ForwardSyncProcessSetting();
            var settiattempt = Configuration.Config.GetSection("ForwardSyncProcessSetting")
                    .GetChildren().Where(x=>x.Key == SpecificProcess.ToString())
                    .Select(x=>
                          new ForwardSyncProcessSetting().InjectFrom(x.GetChildren().ToDictionary(r => r.Key, r => r.Value))
                    ).FirstOrDefault();

            var settings = Configuration.Config.GetSection("ForwardSyncProcessSetting")
                    .GetChildren().Where(x => x.Key == SpecificProcess.ToString());
            foreach (var x in settings) {
                var c = x.GetChildren().ToDictionary(r => r.Key, r => r.Value);
                var vals = new ForwardSyncProcessSetting();
                vals.SqlQueryLimit = Convert.ToInt32(c["SqlQueryLimit"]);
                return vals;
            }

            return null;
        }
    }

    public class ForwardSyncProcessSetting
    {
        public int SqlQueryLimit = 0;
    }
}
