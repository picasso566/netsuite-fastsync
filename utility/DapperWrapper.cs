﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using _2ndswingCommon;
using System.Text;

namespace netsuite_fastsync
{

    public enum DapperSource
    {
        COMA, CP, G2Common, GSVStorefront, PBS, Purchasing, Storefront, SfWeblink, SyncState
    }

    public static class DapperWrapper
    {
        internal static string GetConnectionString(DapperSource source)
        {
            return Configuration.Get(source.ToString() + "_ConnectionString");
        }

        internal static IEnumerable<T> Query<T>(DapperSource source, string sql, object param = null)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString(source)))
            {
                conn.Open();
                return conn.Query<T>(sql, param);
            }
        }

        internal static int Execute(DapperSource source, string sql, object param = null)
        {
            using (SqlConnection conn = new SqlConnection(GetConnectionString(source)))
            {
                conn.Open();
                return conn.Execute(sql, param);
            }
        }

    }
}
