﻿using _2ndswingCommon;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading;
using log4net.Core;
using System.Diagnostics;
using System.IO;
using _2ndswingCommon.services;
using System.Text;

namespace netsuite_fastsync
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        public static int Main(string[] args)
        {
            NetSuiteHelper helper = new NetSuiteHelper();
            ProgramUtility statics = new ProgramUtility();
            ConsoleHelper.Configure();
            System.Net.ServicePointManager.DefaultConnectionLimit = 20;
            ThreadPool.SetMaxThreads(10, 10);

            if (statics.CommandsToRun.Count == 0)
                return 1;
            NetsuiteLookuper lookups = null;

            //Run the processes
            if (statics.ConditionalRun("customers"))
                new CustomerForwardSync(helper).DoForwardSync(statics);

            if (statics.ConditionalRun("vendors"))
                new VendorForwardSync(helper).DoForwardSync(statics);

            if (statics.ConditionalRun("employees"))
                new EmployeeSync(helper).DoForwardSync(statics);
           
            if (statics.ConditionalRun("parentItems") || statics.ConditionalRun("allItems"))
                lookups = new ParentItemForwardSync(helper, lookups).DoForwardSync(statics);

            if (statics.ConditionalRun("usedItems") || statics.ConditionalRun("allItems"))
                lookups = new UsedItemForwardSync(helper, lookups).DoForwardSync(statics);

            if (statics.ConditionalRun("newItems") || statics.ConditionalRun("allItems"))
                lookups = new NewItemForwardSync(helper, lookups).DoForwardSync(statics);

            if (statics.ConditionalRun("gsvPOs"))
                lookups = new GSVPurchaseOrderForwardSync(helper, lookups).DoForwardSync(statics);


            return 0;
        }
    }
}
