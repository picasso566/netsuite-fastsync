﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;

namespace netsuite_fastsync
{
    class EmployeeSync
    {
        NetSuiteHelper _helper;

        public EmployeeSync(NetSuiteHelper netSuiteHelper)
        {
            _helper = netSuiteHelper;
        }

        //PP 20180731 - In the original sync app for historical data there is code to generate 
        //barebones employees that appear on items or POs but were purged. 
        //I have wrapped that process into the view VI_CP_Users
        public void DoForwardSync(ProgramUtility programUtility)
        {
            QueryCounterpointUsers().Select(ToEmployee)
                  .Subscribe(programUtility.CreateBatchedUpsertObserver("employees", programUtility.BatchSize));
        }

        public static NetsuiteRecordWrapper ToEmployee(CounterpointUserPOCO input)
        {
            var firstlast = input.NAM.ValueOrDefault("").NetsuiteFirstLastNameBySpaces();
            var nam = input.NAM;
            var firstName = firstlast.first;
            var lastName = firstlast.last;
            var usrId = input.USR_ID;

            var initials = input.INITIALS;

            if (initials != null && initials.StartsWith("DCB") && initials.Length == 4)
                initials = initials.Replace("DCB", "DB");

            if (initials != null && initials.StartsWith("MPLS") && initials.Length == 5)
                initials = initials.Replace("MPLS", "MP");

            return new NetsuiteRecordWrapper()
            {
                NetsuiteRecord = new Employee()
                {
                    externalId = usrId,
                    entityId = usrId,
                    initials = initials,
                    firstName = firstName,
                    lastName = lastName,
                    //email = input.EMAIL_ADRS_1.GetEmailForNetsuite(),
                    //phone = input.PHONE_1.GetPhoneNumberOrDefault(null),
                    giveAccess = false,
                    giveAccessSpecified = true,
                    isSalesRep = ((input.IS_SLS_REP ?? "N") == "Y"),
                    isSalesRepSpecified = true
                },
                ExternalId = usrId,
                LocalId = usrId,
                RecordType = "employee"
            };
        }

        public static IEnumerable<CounterpointUserPOCO> QueryCounterpointUsers()
        {
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                connection.Open();
                return connection.Query<CounterpointUserPOCO>(@"
                            CREATE TABLE #employees ( LocalId Varchar(250), CurrentHash Varchar(32))
                            INSERT INTO #employees exec NetSuiteGetChangedEmployeesGEN

                            select u.*, ch.CurrentHash from VI_CP_Users u
                            JOIN #employees ch on rtrim(ch.LocalId) = rtrim(u.USR_ID)", new DynamicParameters(), commandTimeout: 3600);
            }
        }

    }
}
