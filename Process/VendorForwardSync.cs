﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using System.Text.RegularExpressions;

namespace netsuite_fastsync
{
    class VendorForwardSync
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        Dictionary<string, VendorCategory> _vendorCategories;
        List<string> _validGSVSites;
        bool typesLoaded = false;

        NetSuiteHelper _helper;

        public VendorForwardSync(NetSuiteHelper netSuiteHelper)
        {
            _helper = netSuiteHelper;
        }

        private void LoadTypeInfo()
        {
            if (!typesLoaded)
            {
                _vendorCategories = _helper.FindAll<VendorCategory>().Where(rec => rec.externalId != null).ToDictionary(rec => rec.externalId, rec => rec);
                _validGSVSites = new GetNetsuiteLookup().ValidGSVSites();
                typesLoaded = true;
            }
        }

        public void DoForwardSync(ProgramUtility programUtility)
        {
            SyncCounterpointVendorCategory(programUtility);

            IEnumerable<CounterpointVendorPOCO> vendors = QueryCounterpointVendors();
            vendors.Select(ToVendorRecord).Subscribe(programUtility.CreateBatchedUpsertObserver("vendors", programUtility.BatchSize));
            vendors.Select(ToContactRecord).Where(c => c != null).Subscribe(programUtility.CreateBatchedUpsertObserver("contacts", programUtility.BatchSize));
        }

        public static void SyncCounterpointVendorCategory(ProgramUtility programUtility)
        {
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                //TODO: Filter these to exclude ones already uploaded (which is all of them)
                connection.Query<CounterpointVendorCategoryPOCO>("select * from VI_CP_VendorCategories")
                    .ToObservable()
                    .Select(
                    c => new NetsuiteRecordWrapper()
                    {
                        NetsuiteRecord = new VendorCategory() { externalId = c.CATEG_COD, name = c.DESCR },
                        ExternalId = c.CATEG_COD,
                        LocalId = c.CATEG_COD,
                        RecordType = "VendorCategory"
                    }
                    )
                    .Subscribe(programUtility.CreateBatchedUpsertObserver("vendor_and_contact_categories", 20));
            }
        }

        public static IEnumerable<CounterpointVendorPOCO> QueryCounterpointVendors()
        {
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                //TODO: Make the query shorter and put it all in a sproc
                connection.Open();
                string sql = @"
                            CREATE TABLE #ChangedVendors (LocalId Varchar(250),CurrentHash Varchar(32))
                            INSERT INTO #ChangedVendors exec NetSuiteGetChangedVendorsFromCpGEN
                            select v.*, ch.CurrentHash from NetSuite_VI_Vendor v JOIN #ChangedVendors ch on rtrim(ch.LocalId) = rtrim(v.vend_no)
                        ";

               return connection.Query<CounterpointVendorPOCO>(sql, new DynamicParameters(), commandTimeout: 3600);
            }
        }


        public NetsuiteRecordWrapper ToVendorRecord(CounterpointVendorPOCO input)
        {
            LoadTypeInfo();

            var vendNo = input.vend_no;
            var nam = input.nam;
            nam = new Regex("[ ]{2,}", RegexOptions.None).Replace(nam, " ").Trim();
            var categcod = input.categ_cod ?? "";
            var vendcountry = input.cntry.GetCountry();

            var addr = new Address
            {
                addr1 = input.adrs_1.ValueOrDefaultTrimmedNullable(""),
                addr2 = input.adrs_2.ValueOrDefaultTrimmedNullable(""),
                addr3 = input.adrs_3.ValueOrDefaultTrimmedNullable(""),
                city = input.city.ValueOrDefaultTrimmedNullable(""),
                country = input.cntry.GetCountry(),
                countrySpecified = true,
                state = input.state.ValueOrDefaultTrimmedNullable(""),
                zip = input.zip_cod.ValueOrDefaultTrimmedNullable("")
            };

            var vend = new Vendor
            {
                customForm = new RecordRef()
                {
                    internalId = Configuration.Get("FormVendor")
                },

                entityId = string.Format("{0} ({1})", nam, vendNo),
                externalId = "VN-" + vendNo,
                companyName = nam, // string.Format("{0} ({1})", nam, vendNo),
                isPerson = false,
                email = input.email_adrs_1.GetEmailForNetsuite(),
                phone = input.phone_1.GetPhoneNumberOrDefault(null),
                altPhone = input.phone_2.GetPhoneNumberOrDefault(null),
                addressbookList = new VendorAddressbookList
                {
                    // if you don't have this set to true (the default), an upsert really isn't going to work for ya... it'll add a new address each time.
                    replaceAll = true,
                    addressbook = new VendorAddressbook[]
                       {
                            new VendorAddressbook
                            {
                                addressbookAddress = addr
                                , defaultBilling = true, defaultBillingSpecified = true
                                , defaultShipping = true, defaultShippingSpecified = true
                            }
                       }
                }
            };

            if (_vendorCategories.ContainsKey(categcod))
            {
                var vc = _vendorCategories[categcod];
                vend.category = new RecordRef
                {
                    type = RecordType.vendorCategory,
                    internalId = vc.internalId,
                    externalId = vc.externalId
                };
            }

            var comment = string.Empty;
            var c1 = input.contct_1.ValueOrDefault(string.Empty);
            var c2 = input.contct_2.ValueOrDefault(string.Empty);
            var e1 = input.email_adrs_1.ValueOrDefault(string.Empty);
            var e2 = input.email_adrs_2.ValueOrDefault(string.Empty);

            if (c1.Length > 0)
                comment += string.Format("Old Contact 1 Field:{0}", c1);
            if (c2.Length > 0)
                comment += ((comment.Length > 0) ? "\n" : "") + string.Format("Old Contact 2 Field:{0}", c2);
            if (e1.Length > 0)
                comment += ((comment.Length > 0) ? "\n" : "") + string.Format("Old Email 1 Field:{0}", e1);
            if (e2.Length > 0)
                comment += ((comment.Length > 0) ? "\n" : "") + string.Format("Old Email 2 Field:{0}", e2);

            if (comment.Length > 0)
                vend.comments = comment;

            if (input.prof_dat_1 != null)
                vend.dateCreated = input.prof_dat_1.GetValueOrDefault(DateTime.Now);

            // set any custom fields
            List<CustomFieldRef> custFields = new List<CustomFieldRef>();
            var email2 = input.email_adrs_2.GetEmailForNetsuite();
            if (email2 != null)
            {
                custFields.Add(new StringCustomFieldRef()
                {
                    scriptId = "custentity4",
                    value = email2
                });
            }

            if (input.SiteGUID != null && _validGSVSites.Contains(input.SiteGUID))
                input.SiteGUID.CustomRecordFieldLookupValue(_helper, "customrecord_gsv_site", "custentity_gsvsite_ref", custFields);

            if (custFields.Count() > 0)
                vend.customFieldList = custFields.ToArray();

            // i, ct = individual OR f/l name = individual, all others are companies.
            var firstName = input.fst_nam.ValueOrDefault(null);
            var lastName = input.lst_nam.ValueOrDefault(null);
            var hasPersonName = firstName != null && lastName != null;


            if (categcod == "G" && hasPersonName)
            {
                vend.email = null;
                vend.phone = null;
                vend.altEmail = null;
                vend.altPhone = null;
            }
            else
            {
                // only set these fields if they're an individual.
                if (categcod == "I" || categcod == "CT" || hasPersonName)
                {
                    // individual vendor type
                    vend.isPerson = true;
                    vend.firstName = firstName;
                    vend.lastName = lastName;
                    //Always errors. Does this need to match a list of acceptable salutations?
                    //vend.salutation = input.GetOrDefault<string>("salutation", null);
                }
            }
            return new NetsuiteRecordWrapper() { RecordType = "vendor", NetsuiteRecord = vend, LocalId = vendNo, ExternalId = ((Vendor)vend).externalId, LocalHash = input.CurrentHash.ValueOrDefault(string.Empty)};
        }

        public NetsuiteRecordWrapper ToContactRecord(CounterpointVendorPOCO input)
        {
            var vendNo = input.vend_no;
            var nam = input.nam;
            nam = new Regex("[ ]{2,}", RegexOptions.None).Replace(nam, " ").Trim();

            var categcod = input.categ_cod ?? "";

            // i, ct = individual OR f/l name = individual, all others are companies.
            var firstName = input.fst_nam.ValueOrDefault(null);
            var lastName = input.lst_nam.ValueOrDefault(null);
            var hasPersonName = firstName != null && lastName != null;


            Contact newContact = null;

            // if g, and has f/l name, add a related contact.
            if (categcod == "G" && hasPersonName)
            {
                newContact = new Contact
                {
                    customForm = new RecordRef()
                    {
                        internalId = Configuration.Get("FormContact")
                    },
                    externalId = "PC-" + vendNo,
                    firstName = firstName,
                    lastName = lastName,
                    email = input.email_adrs_1.GetEmailForNetsuite(),
                    phone = input.phone_1.GetPhoneNumberOrDefault(null),
                    salutation = input.salutation.ValueOrDefault(null),
                    altEmail = input.email_adrs_2.GetEmailForNetsuite(),
                    officePhone = input.phone_2.GetPhoneNumberOrDefault(null),

                    categoryList = new RecordRef[]
                    {
                        new RecordRef()
                        {
                            externalId = "GolfPro",
                            type = RecordType.contactCategory,
                            typeSpecified = true
                        }
                    },

                    company = new RecordRef
                    {
                        externalId = "VN-" + vendNo,
                        type = RecordType.vendor,
                        typeSpecified = true,
                        name = "vendorRef"
                    }
                };
            }
            if (newContact != null)
                return new NetsuiteRecordWrapper() { NetsuiteRecord = newContact, LocalId = vendNo, ExternalId = ((Contact)newContact).externalId, RecordType = "contact" };

            return null;
        }
    }
}
