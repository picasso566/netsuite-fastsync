﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;

namespace netsuite_fastsync
{
    class UsedItemForwardSync
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        NetsuiteLookuper lookups;
        NetSuiteHelper _helper;


        public UsedItemForwardSync(NetSuiteHelper netSuiteHelper, NetsuiteLookuper lookups)
        {
            _helper = netSuiteHelper;
            this.lookups = lookups;
        }

        private void LoadTypeInfo()
        {
            //Item Lookups only get created if there are items to process
            if (lookups == null)
            {
                lookups = new NetsuiteLookuper(_helper);
                lookups.PopulateItemLookups();
            }
        }

        public NetsuiteLookuper DoForwardSync(ProgramUtility programUtility)
        {
            QueryUsedItems()
               .Select(ToUsedItem)
               .Where(r => r != null)
               .Subscribe(programUtility.CreateBatchedUpsertObserver("UsedItem", programUtility.BatchSize));
            return lookups;
        }

        public static IEnumerable<UsedItemPOCO> QueryUsedItems()
        {
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                connection.Open();
                string sql = @"
                            CREATE TABLE #ChangedUsedItems ( LocalId Varchar(250), CurrentHash Varchar(32) )
                            INSERT INTO #ChangedUsedItems exec NetSuiteGetChangedUsedItemsGEN
                            select v.*, ch.CurrentHash from NetSuite_VI_ItemUsed v JOIN #ChangedUsedItems ch on rtrim(ch.LocalId) = rtrim(v.CpItemNo)
                        ";

                return connection.Query<UsedItemPOCO>(sql, new DynamicParameters(), commandTimeout: 3600);
            }
        }
   
        public NetsuiteRecordWrapper ToUsedItem(UsedItemPOCO input)
        {
            LoadTypeInfo();

            var item = new InventoryItem()
            {
                itemId = input.CpItemNo,
                externalId = input.CpItemNo,
                salesDescription = (input.OnlineTitle.IsNullOrEmpty()) ? input.CpItemNo : input.OnlineTitle,
                storeDescription = input.OnlineTitle.MaximumLengthString(2000),
                cogsAccount = lookups.CogsAccount,
                assetAccount = lookups.AssetAccount,
                @class = (input.BuyType == "O") ? lookups.OpportunityClass : lookups.UsedClass,
                upcCode = input.UPC,
                parent = new RecordRef() { type = RecordType.inventoryItem, typeSpecified = true, externalId = input.NsParentItemSku }
            };
            List<CustomFieldRef> custFields = new List<CustomFieldRef>();
            input.FreeNotes.MaximumLengthString(300).CustomFieldStringValue("custitem_g2_freenotes", custFields);
            input.EbayTitle.CustomFieldStringValue("custitem_g2_ebaytitle", custFields);
            input.OnlineTitle.CustomFieldStringValue("custitem_g2_onlinetitle", custFields);
            input.StyleNo.CustomFieldStringValue("custitem_g2_styleno", custFields);
            //input.DateVerified.CustomFieldDateValue("custitem_g2_dateverified", custFields);
            input.PricePremiumAmt.CustomFieldFloatValue("custitem_g2_pricepremiumamt", custFields);
            input.PricePremiumPct.CustomFieldFloatValue("custitem_g2_pricepremiumpct", custFields);
            input.Loft.CustomFieldFloatValue("custitem_g2_club_loft", custFields);
            input.NumberOfGolfBalls.CustomFieldIntValue("custitem_g2_numberofgolfballs", custFields);
            ((input.Gripped ?? "N").ToUpper() == "Y").CustomFieldCheckboxValue("custitem_g2_shaft_gripped", custFields);
            ((input.OkForAz ?? "N").ToUpper() == "Y").CustomFieldCheckboxValue("custitem_g2_okforaz", custFields);
            ((input.IsEcommerceItem ?? "N").ToUpper() == "Y").CustomFieldCheckboxValue("custitem_g2_isecommerceitem", custFields);
            ((input.ListOn2SOnly ?? "N").ToUpper() == "Y").CustomFieldCheckboxValue("custitem_g2_liston2sonly", custFields);
            ((input.HasImage ?? "N").ToUpper() == "Y").CustomFieldCheckboxValue("custitem_g2_hasimage", custFields);
            input.SubCategoryId.CustomItemFieldLookupValue(_helper, "customrecord_g2_subcategory", "custitem_g2_subcategory_ref", custFields);
            input.BounceId.CustomItemFieldLookupValue(_helper, "customrecord_g2_bounce", "custitem_g2_bounce_ref", custFields);
            input.DexterityId.CustomItemFieldLookupValue(_helper, "customrecord_g2_dexterity", "custitem_g2_dexterity_ref", custFields);
            input.LieAngleId.CustomItemFieldLookupValue(_helper, "customrecord_g2_lieangle", "custitem_g2_lieangle_ref", custFields);
            input.ShaftFlexId.CustomItemFieldLookupValue(_helper, "customrecord_g2_shaft_flex", "custitem_g2_shaft_flex_ref", custFields);
            input.ShaftTypeId.CustomItemFieldLookupValue(_helper, "customrecord_g2_shaft_type", "custitem_g2_shaft_type_ref", custFields);
            input.LocationId.CustomItemFieldLookupValue(_helper, "customrecord_g2_location", "custitem_g2_location_ref", custFields);
            input.ConditionId.CustomItemFieldLookupValue(_helper, "customrecord_g2_condition", "custitem_g2_condition_ref", custFields);

            foreach (var attribute in lookups.ItemAttributeNames)
                foreach (var productGroupName in attribute.Value.Where(x => x == input.GsProductGroupCode.ToLower()))
                    NSExtMethods.SetGSFieldByName(input, _helper, attribute.Key, productGroupName, lookups.ItemAttibutes, custFields);

            ////special case for clubs and shafts. Both have the same Length dropdown:
            if (input.GsProductGroupCode == "Club" || input.GsProductGroupCode == "Shaft")
            {
                input.FormattedLength.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_shaft_length", "custitem_g2_club_length_ref", lookups.ItemAttibutes, custFields);
                input.FormattedLoft.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_club_loft", "custitem_g2_club_loft_ref", lookups.ItemAttibutes, custFields);
                input.Length.CustomFieldFloatValue("custitem_g2_club_length", custFields);
            }
            item.customFieldList = custFields.ToArray();
            var pricing = new List<Pricing>();
            input.OutOfStateSalePrice.AddItemPriceToPriceList("Out Of State Price", lookups.PriceLevels, pricing);
            input.MSRP.AddItemPriceToPriceList("MSRP", lookups.PriceLevels, pricing);
            input.SellPrice.AddItemPriceToPriceList("Base Price", lookups.PriceLevels, pricing);
            input.Cost.AddItemPriceToPriceList("Bid Price", lookups.PriceLevels, pricing);
            input.ClearancePrice.AddItemPriceToPriceList("Clearance Price", lookups.PriceLevels, pricing);

            if (pricing.Count > 0)
                item.pricingMatrix = new PricingMatrix() { pricing = pricing.ToArray(), replaceAll = true };
            else
                log.Error("Item has no prices! " + item.externalId);

            return new NetsuiteRecordWrapper() { NetsuiteRecord = item, ExternalId = input.CpItemNo, LocalId = input.CpItemNo, LocalHash = input.CurrentHash, RecordType = "inventoryItem" };
        }
    }
}
