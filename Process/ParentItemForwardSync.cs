﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;

namespace netsuite_fastsync
{
    class ParentItemForwardSync
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        NetsuiteLookuper lookups;
        NetSuiteHelper _helper;

        public ParentItemForwardSync(NetSuiteHelper netSuiteHelper, NetsuiteLookuper lookups)
        {
            _helper = netSuiteHelper;
            this.lookups = lookups;
        }

        private void LoadTypeInfo()
        {
            //Item Lookups only get created if there are items to process
            if (lookups == null)
            {
                lookups = new NetsuiteLookuper(_helper);
                lookups.PopulateItemLookups();
            }
        }

        public NetsuiteLookuper DoForwardSync(ProgramUtility programUtility)
        {
            var items = QueryParentItems();
            if (items.Any())
            {
                LoadTypeInfo();

                items
                    .Select(ToParentItem)
                    .Where(r => r != null)
                    .Subscribe(programUtility.CreateBatchedUpsertObserver("ParentItems", programUtility.BatchSize));
            }
            return lookups;
        }

        public static IEnumerable<ParentItemPOCO> QueryParentItems()
        {
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                connection.Open();
                string sql = @"
                        SET ROWCOUNT 50000
                            CREATE TABLE #ChangedParentItems ( LocalId Varchar(250), CurrentHash Varchar(32) )
                            INSERT INTO #ChangedParentItems exec NetSuiteGetChangedParentItemsGEN
                            select v.*, ch.CurrentHash from NetSuite_VI_ParentItem v JOIN #ChangedParentItems ch on rtrim(ch.LocalId) = rtrim(v.SKU)
                        ";

                return connection.Query<ParentItemPOCO>(sql, new DynamicParameters(), commandTimeout: 3600);
            }
        }


        public NetsuiteRecordWrapper ToParentItem(ParentItemPOCO input)
        {
            var sku = input.SKU;
            var name = input.name; //var name = (input.name != null) ? ((sku + ":" + input.name).MaximumLengthString(59)) : sku;
            var summary = input.Summary;

            var model = new InventoryItem
            {
                externalId = sku,
                itemId = sku,
                nullFieldList = new string[] { "displayName" },
                // this shows up in the UI as "sales description" on the sales/pricing tab.
                salesDescription = input.salesDescription, // input.salesDescription.MaximumLengthString(998),
                // this shows up in the UI as "web store description" on the "web store" tab.
                storeDescription = input.storeDecription,
                cogsAccount = lookups.CogsAccount,
                assetAccount = lookups.AssetAccount
            };

            if (input.NewKitGUID != null && input.NewKitExists == 0)
            {
                log.ErrorFormat("No NewKit {0} for Parent Item {1}", input.NewKitGUID.ToString(), input.SKU);
                return null;
            }

            if (input.UsedKitGUID != null && input.UsedKitExists == 0)
            {
                log.ErrorFormat("No UsedKit {0} for Parent Item {1}", input.UsedKitGUID.ToString(), input.SKU);
                return null;
            }

            List<CustomFieldRef> custFields = new List<CustomFieldRef>();

            if (input.UsedKitExists == 1)
                input.UsedKitGUID.CustomFieldKitServiceItemReference("custitem_customization_used_ref", custFields);
            if (input.NewKitExists == 1)
                input.NewKitGUID.CustomFieldKitServiceItemReference("custitem_customization_new_ref", custFields);

            input.BidPrice.CustomFieldCurrencyValue("custitem_g2_basebidprice", custFields);
            input.BrandId.CustomItemFieldLookupValue(_helper, "customrecord_g2_brand", "custitem_g2_brand_ref", custFields);
            input.CategoryId.CustomItemFieldLookupValue(_helper, "customrecord_g2_category", "custitem_g2_category_ref", custFields);
            input.ClubDesiredBallFlight.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_club_desiredballflight", "custitem_g2_club_desiredballflight_ref", lookups.ItemAttibutes, custFields);
            input.ClubHeadSize.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_club_headsize", "custitem_g2_club_headsize_ref", lookups.ItemAttibutes, custFields);
            input.ClubManufacturingType.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_club_manufacturingtype", "custitem_g2_club_manufacturingtype_ref", lookups.ItemAttibutes, custFields);
            input.PutterHeadStyle.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_club_putterheadstyle", "custitem_g2_club_putterheadstyle_ref", lookups.ItemAttibutes, custFields);
            input.ClubSpinRate.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_club_spinrate", "custitem_g2_club_spinrate_ref", lookups.ItemAttibutes, custFields);
            input.ToeHang.CustomItemFieldLookupValueFromList(_helper, "customrecord_g2_club_toehang", "custitem_g2_club_toehang_ref", lookups.ItemAttibutes, custFields);

            input.name.CustomFieldStringValue("custitem_g2_name", custFields);
            input.ProdSynonyms.CustomFieldStringValue("custitem_g2_prodsynonyms", custFields);
            input.RetailNewPrice.CustomFieldCurrencyValue("custitem_g2_retailnewprice", custFields);
            input.SalesRepPrice.CustomFieldCurrencyValue("custitem_g2_salesrepprice", custFields);
            input.SKU.CustomFieldStringValue("custitem_g2_sku", custFields);
            input.Summary.CustomFieldStringValue("custitem_g2_summary", custFields);
            input.TechConstruction.CustomFieldStringValue("custitem_g2_techconstruction", custFields);
            input.ClubYearReleased.CustomFieldIntValue("custitem_g2_yearrelease", custFields);

            ((input.OkForAz ?? "N") == "Y").CustomFieldCheckboxValue("custitem_g2_okforaz", custFields);
            ((input.OkForEb ?? "N") == "Y").CustomFieldCheckboxValue("custitem_g2_okforeb", custFields);
            ((input.OkForSf ?? "N") == "Y").CustomFieldCheckboxValue("custitem_g2_okforsf", custFields);
            ((input.OkForStores ?? "N") == "Y").CustomFieldCheckboxValue("custitem_g2_okforstores", custFields);
            ((input.EndOfLife ?? "N") == "Y").CustomFieldCheckboxValue("custitem_g2_endoflife", custFields);

            List<ListOrRecordRef> dsRefs = new List<ListOrRecordRef>();
            var shaftLookup = lookups.ItemAttibutes["customrecord_g2_shaft_type"];
            for (var dsNum = 1; dsNum < 6; dsNum++)
            {
                var fVal = (input.GetType().GetProperty("DefaultShaft_" + dsNum).GetValue(input) ?? string.Empty).ToString();
                if (!fVal.IsNullOrEmpty())
                {
                    if (fVal == "0") fVal = "0 SHAFT";
                    var key = fVal.ToLower();
                    if (shaftLookup.ContainsKey(key))
                        dsRefs.Add(new ListOrRecordRef() { typeId = "customrecord_g2_shaft_type", internalId = shaftLookup[key] });
                    else
                        dsRefs.Add(new ListOrRecordRef() { typeId = "customrecord_g2_shaft_type", externalId = fVal });
                }
            }
            if (dsRefs.Count > 0)
                custFields.Add(new MultiSelectCustomFieldRef() { scriptId = "custitem_g2_default_shaft_model_refs", value = dsRefs.ToArray() });

            // ONLY new iron sets have this value populated. So only those will have values, as well as default irons attribute.
            if (!input.IronSetClubs.IsNullOrEmpty())
            {
                var clubNos = input.IronSetClubs.Split(',');
                List<ListOrRecordRef> clubNoRefs = new List<ListOrRecordRef>();
                foreach (string clubNoDirty in clubNos)
                    clubNoRefs.Add(new ListOrRecordRef() { typeId = "customrecord_g2_club_number", externalId = clubNoDirty.Trim().ToUpper() });

                if (clubNoRefs.Count > 0)
                {
                    custFields.Add(new MultiSelectCustomFieldRef() { scriptId = "custitem_g2_ironsetclubs_refs", value = clubNoRefs.ToArray() });
                    if (clubNoRefs.Count > 1)
                    {
                        // if this is an iron set, add the default club refs as well.
                        List<ListOrRecordRef> defaultClubRefs = new List<ListOrRecordRef>();
                        foreach (string clubNoDirty in lookups.DefaultIrons)
                            defaultClubRefs.Add(new ListOrRecordRef() { typeId = "customrecord_g2_club_number", externalId = clubNoDirty.Trim().ToUpper() });
                        custFields.Add(new MultiSelectCustomFieldRef() { scriptId = "custitem_g2_ironsetdefaultclubs_refs", value = defaultClubRefs.ToArray() });
                    }

                }
                if (clubNoRefs.Count <= 1)
                    custFields.Add(new MultiSelectCustomFieldRef() { scriptId = "custitem_g2_ironsetdefaultclubs_refs", value = new ListOrRecordRef[] { } });
            }

            input.ItemTypeId.ToString().CustomItemFieldLookupValue(_helper, "customrecord_g2_itemtype", "custitem_g2_itemtype_ref", custFields);
            input.ModelId.CustomItemFieldLookupValue(_helper, "customrecord_g2_model", "custitem_g2_model_ref", custFields);

            var vidRefs = NSExtMethods.VideoFieldsToListOfRefs(lookups, input);
            if (vidRefs.Count > 0)
            {
                var vids = new MultiSelectCustomFieldRef() { scriptId = "custitem_g2_video_refs", value = vidRefs.ToArray() };
                custFields.Add(vids);
            }

            var pricing = new List<Pricing>();
            input.RetailNewPrice.AddItemPriceToPriceList("Base Price", lookups.PriceLevels, pricing);
            input.SalesRepPrice.AddItemPriceToPriceList("Sales Rep Price", lookups.PriceLevels, pricing);
            input.BidPrice.AddItemPriceToPriceList("Bid Price", lookups.PriceLevels, pricing);

            if (pricing.Count > 0)
                model.pricingMatrix = new PricingMatrix() { pricing = pricing.ToArray(), replaceAll = true };

            model.customFieldList = custFields.ToArray();
            return new NetsuiteRecordWrapper() { NetsuiteRecord = model, ExternalId = sku, LocalId = sku, LocalHash = input.CurrentHash, RecordType = "inventoryItem" };
        }
        
    }
}
