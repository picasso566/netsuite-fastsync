﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;
using System.Text.RegularExpressions;

namespace netsuite_fastsync
{
    class GSVPurchaseOrderForwardSync
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        NetsuiteLookuper lookups;
        NetSuiteHelper _helper;

        public GSVPurchaseOrderForwardSync(NetSuiteHelper netSuiteHelper, NetsuiteLookuper lookups)
        {
            _helper = netSuiteHelper;
            this.lookups = lookups;
            this.lookups.PopulatePOAccounts();
        }

        private void LoadTypeInfo()
        {
            if (lookups == null)
                lookups = new NetsuiteLookuper(_helper);
            lookups.PopulatePOAccounts();
        }

        public NetsuiteLookuper DoForwardSync(ProgramUtility programUtility)
        {
            QueryGSVPOs().Select(ToGSVPurchaseOrder).Where(r => r != null)
                .Subscribe(programUtility.CreateBatchedUpsertObserver("GSVPurchaseOrder", programUtility.BatchSize));

            return lookups;
        }

        public static IEnumerable<GSVPurchaseOrderDto> QueryGSVPOs()
        {
            var processSetting = ForwardSyncProcessSettings.GetProcessSetting(ForwardSyncProcessNames.GSVPurchaseOrder);
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                connection.Open();
                string sql = "CREATE TABLE #ChangedGSVPOs ( LocalId Varchar(250), CurrentHash Varchar(32) ) " +
                            "INSERT INTO #ChangedGSVPOs exec NetSuiteGetChangedGSVPOsGEN " +
                                            ((processSetting != null && processSetting.SqlQueryLimit > 0) ? processSetting.SqlQueryLimit.ToString() : string.Empty) +
                            "select v.*, ch.CurrentHash from NetSuite_VI_GSVPOHeaderLinesJoined v JOIN #ChangedGSVPOs ch on rtrim(ch.LocalId) = rtrim(v.SKU)";

                List<GSVPurchaseOrderPOCO> pocoResult = connection.Query<GSVPurchaseOrderPOCO>(sql, new DynamicParameters(), commandTimeout: 3600).ToList();
                List<GSVPurchaseOrderDto> result = new List<GSVPurchaseOrderDto>();

                var POs = pocoResult.ToLookup(p => p.PoNo);
                foreach (var p in POs)
                {
                    var po = Mapper.Map<GSVPurchaseOrderPOCO, GSVPurchaseOrderDto>(p.First());
                    po.Lines = p.Select(l => Mapper.Map<GSVPurchaseOrderPOCO, GSVPurchaseOrderLineDto>(l)).ToList();
                    result.Add(po);
                }
                return result.AsEnumerable();
            }
        }

        public NetsuiteRecordWrapper ToGSVPurchaseOrder(GSVPurchaseOrderDto input)
        {
            LoadTypeInfo();
            var po = new PurchaseOrder();
            po.customForm = new RecordRef() { internalId = Configuration.Get("FormPO") };
            po.approvalStatus = new RecordRef() { internalId = "2" };

            var poNumber = input.PoNo;
            var newPoNumber = "PO-" + poNumber;

            po.tranId = newPoNumber;
            po.tranDate = input.PoDate ?? DateTime.Now;
            po.tranDateSpecified = true;
            po.externalId = newPoNumber;

            //These were commented in original, do we want to comment them?
            //po.source = input.Source.ValueOrDefault(null);

            po.memo = input.InternalNotes.ValueOrDefault(null);
            po.entity = input.VendNo.ExternalIdToRecordRef("VN-", RecordType.vendor);

            List<CustomFieldRef> custFields = new List<CustomFieldRef>();
            ((input.IsFreeShipping ?? "N") == "Y").CustomFieldCheckboxValue("custbody_g2_isfreeshipping", custFields);
            ((input.SentToPassPort ?? "N") == "Y").CustomFieldCheckboxValue("custbody_bf_prepaidpo", custFields);
            input.PriceLevelId.CustomItemFieldLookupValue(_helper, "customrecord_pricelevel", "custbody_pricelevel_ref", custFields);
            input.WidgetId.CustomItemFieldLookupValue(_helper, "customrecord_widget_type", "custbody_widgettype_ref", custFields);
            input.TagName.CustomFieldStringValue("custbody_tagname", custFields);
            (input.WebOrderId.ToString() ?? string.Empty).CustomFieldStringValue("custbody_weborderid", custFields);
            input.PoNo.CustomFieldStringValue("custbody_cp_ponum", custFields);

            if (input.SiteGUIDExists == 1 && input.SiteGUID != null && input.SiteGUID != Guid.Parse("00000000-0000-0000-0000-000000000000"))
                input.SiteGUID.ToString().CustomItemFieldLookupValue(_helper, "customrecord_gsv_site", "custbody_gsvsite_ref", custFields);

            //If TradeOrExcess = I then true
            ((input.TradeOrExcess ?? "NULL") == "I").CustomFieldCheckboxValue("custbody_isexcess", custFields);

            //Original code: SitePayCode is never set anywhere:
            //string paycode = input.GetOrDefault("SitePayCode", string.Empty);
            //if (paycode != string.Empty)
            //    input.Set("_paycode", paycode);
            //helper.CustomRecordFieldLookupValue("customrecord_paycode", "custbody_paycode_ref", "_paycode", input, custFields);

            if (input.UserId.ValueOrDefault(string.Empty) != string.Empty && input.UserExists == 1)
            {
                custFields.Add(new SelectCustomFieldRef()
                {
                    scriptId = "custbody_employee_ref",
                    value = new ListOrRecordRef()
                    {
                        externalId = new Regex("[^a-zA-Z0-9 -]").Replace(input.UserId.ValueOrDefault(string.Empty).Trim(), "").ToUpper(),
                        typeId = "-4"
                    }
                });
            }

            input.Source.CustomFieldStringValue("custbody_g2_source", custFields);
            po.customFieldList = custFields.ToArray();

            var lines = new List<PurchaseOrderItem>();

            foreach (var poLine in input.Lines)
            {
                var lineItemCustFields = new List<CustomFieldRef>();

                poLine.Line_ExpClubsInIronSet.CustomFieldIntValue("custcol_g2_clubsinironset", lineItemCustFields);
                poLine.Line_ExpClubNoId.CustomFieldIntValue("custcol_g2_club_number", lineItemCustFields);
                poLine.Line_QtyExpected.CustomFieldFloatValue("custcol_expectedqty", lineItemCustFields);
                poLine.Line_ShaftFlexId.CustomItemFieldLookupValue(_helper, "customrecord_g2_shaft_flex", "custcol_g2_shaft_flex_ref", lineItemCustFields);

                if (poLine.Line_ExpectedConditionId != null && (poLine.Line_ExpectedConditionId ?? 0) > 0)
                {
                    lineItemCustFields.Add(
                             new SelectCustomFieldRef()
                             {
                                 scriptId = "custcol_g2_condition_ref",
                                 value = new ListOrRecordRef()
                                 {
                                     typeId = lookups.ConditionRef.internalId,
                                     internalId = lookups.ItemAttibutes["customrecord_g2_condition"][(poLine.Line_ExpectedConditionId ?? 0).ToString()]
                                 }
                             });
                }
                else
                    log.WarnFormat("PO {0} no expected condition found for {1}, line sequence {2}", input.PoNo, (poLine.Line_ExpectedConditionId ?? 0).ToString(), poLine.Line_PoLineSeqNo.ToString());

                if (!poLine.Line_UserId.IsNullOrEmpty() && poLine.UserExists == 1)
                {
                    lineItemCustFields.Add(
                           new SelectCustomFieldRef()
                           {
                               scriptId = "custcol_employee_ref",
                               value = new ListOrRecordRef()
                               {
                                   externalId = poLine.Line_UserId.ToUpper(),
                                   typeId = "-4"
                               }
                           });
                }

                // the PO qty needs to match item recipt, so, use that if we have it.
                // before being received, we'll get the expected qty? then recived, PO will update...
                var qty = poLine.Line_QtyReceived.GetValueOrDefault(poLine.Line_QtyExpected.GetValueOrDefault(0));
                var newLine = new PurchaseOrderItem()
                {
                    description = poLine.Line_Description.ValueOrDefault(null),
                    item = new RecordRef() { type = RecordType.inventoryItem, typeSpecified = true, externalId = poLine.externalItemId },
                    quantity = qty,
                    quantitySpecified = true,
                    customFieldList = lineItemCustFields.ToArray()
                };
                if (qty > 0)
                {
                    double expectedPrice = 0.00;
                    if (poLine.Line_ExpectedPrice != null && (poLine.Line_ExpectedPrice ?? 0) > 0)
                    {
                        expectedPrice = (double)(poLine.Line_ExpectedPrice ?? 0);
                        expectedPrice = Math.Round(expectedPrice, 2);
                        newLine.amount = expectedPrice;
                        newLine.amountSpecified = true;
                    }

                    if (poLine.Line_ExpUnitPrice != null && (poLine.Line_ExpUnitPrice ?? 0) > 0)
                        newLine.rate = Math.Round((double)(poLine.Line_ExpectedPrice ?? 0), 2).ToString();
                    else
                        if (expectedPrice > 0)
                        newLine.rate = Math.Round(expectedPrice / qty, 2).ToString();
                    else
                        newLine.rate = "0";
                }
                else
                {
                    newLine.amount = 0.0;
                    newLine.amountSpecified = true;
                }
                lines.Add(newLine);
            }
            po.itemList = new PurchaseOrderItemList()
            {
                replaceAll = true,
                item = lines.ToArray()
            };

            if (input.ShippingCharges != null && (input.ShippingCharges ?? 0) > 0)
            {
                po.expenseList = new PurchaseOrderExpenseList()
                {
                    expense = new PurchaseOrderExpense[] {
                        new PurchaseOrderExpense(){
                            amount = ((double)(input.ShippingCharges ?? 0)) * -1,
                            amountSpecified = true,
                            account = lookups.shippingAccount,
                            customFieldList = new CustomFieldRef[]{
                                new LongCustomFieldRef(){
                                    scriptId = "custcol_numshippinglabels",
                                    value = input.ShippingLabels.GetValueOrDefault(0)
                                }
                            }
                        }
                    },
                    replaceAll = true
                };

            }
            return new NetsuiteRecordWrapper() { NetsuiteRecord = po, ExternalId = newPoNumber, LocalId = input.PoNo, LocalHash = input.CurrentHash, RecordType = "purchaseOrder" };
        }
    }
}
