﻿using _2ndswingCommon;
using Dapper;
using Dapper.Contrib;
using Omu.ValueInjecter;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;
using System.Reactive.Linq;
using System.Text;
using System.Linq;
using System.Collections.ObjectModel;
using _2ndswingCommon.SuiteTalk_2016_2_0;

namespace netsuite_fastsync
{
    class CustomerForwardSync
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        Dictionary<string, CustomerCategory> _customerCategories;
        Dictionary<string, CustomerStatus> _customerStatus;
        NetSuiteHelper _helper;

        bool typesLoaded = false;
        RecordRef _defaultStatus;
        RecordRef _customerLostStatus;
        RecordRef _noOrderStatus;
        List<string> _validGSVSites;

        public CustomerForwardSync(NetSuiteHelper netSuiteHelper)
        {
            _helper = netSuiteHelper;
            _customerCategories = _helper.FindAll<CustomerCategory>().ToDictionary(rec => rec.externalId, rec => rec);
        }

        private void LoadTypeInfo()
        {
            if (!typesLoaded)
            {
                _customerStatus = _helper.FindAll<CustomerStatus>().ToDictionary(rec => rec.name, rec => rec);
                _defaultStatus = _helper.RecordToRef(_customerStatus["Active"]);
                _customerLostStatus = _helper.RecordToRef(_customerStatus["Lost Customer"]);
                _noOrderStatus = _helper.RecordToRef(_customerStatus["Proposal"]);
                _validGSVSites = new GetNetsuiteLookup().ValidGSVSites();
                typesLoaded = true;
            }
        }


        public void DoForwardSync(ProgramUtility programUtility)
        {
            var cats = SyncCounterpointCustomerCategory(programUtility);
            cats.Select(c => new NetsuiteRecordWrapper()
            {
                RecordType = "CustomerCategory",
                ExternalId = c.CATEG_COD,
                LocalId = c.CATEG_COD,
                NetsuiteRecord = new CustomerCategory() { externalId = c.CATEG_COD, name = c.DESCR }
            }).ToObservable().Subscribe(programUtility.CreateBatchedUpsertObserver("customer_categories", 20));


            QueryCounterpointCustomers().Select(ToCustomer).Where(r => r != null)
                   .Subscribe(programUtility.CreateBatchedUpsertObserver("cp_customers", programUtility.BatchSize));
        }

        public static IEnumerable<CounterpointCustomerCategoryPOCO> SyncCounterpointCustomerCategory(ProgramUtility programUtility)
        {
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                return connection.Query<CounterpointCustomerCategoryPOCO>("select * from VI_CP_CustomerCategories");
            }
        }

        public static IEnumerable<CounterpointCustomerPOCO> QueryCounterpointCustomers()
        {
            using (SqlConnection connection = new SqlConnection(DapperWrapper.GetConnectionString(DapperSource.SyncState)))
            {
                connection.Open();
                string customerSql = @"
                            CREATE TABLE #ChangedCustomers ( LocalId Varchar(250), CurrentHash Varchar(32) )
                            INSERT INTO #ChangedCustomers exec NetSuiteGetChangedCustomersFromCpGEN 

                            select c.*, ch.CurrentHash from NetSuite_VI_Customer c join  #ChangedCustomers ch on ch.LocalId = c.CustomerId order by c.CustomerId 
                        ";
                string addressSql = @"
                        SELECT *, 1 as IsDefault  FROM NetSuite_VI_CustomerBillToAddress 
                        WHERE CustomerId IN (SELECT LocalId FROM #ChangedCustomers)
                        ORDER BY CustomerId
                    ";

                List<CounterpointCustomerPOCO> custs = connection.Query<CounterpointCustomerPOCO>(customerSql, new DynamicParameters(), commandTimeout: 3600).ToList();
                log.Info("Querying Customers Complete: " + custs.Count.ToString());
                List<CounterpointCustomerAddressPOCO> adds = connection.Query<CounterpointCustomerAddressPOCO>(addressSql, new DynamicParameters(), commandTimeout: 3600).ToList();
                log.Info("Querying Customer Addresses Complete: " + adds.Count.ToString());

                custs.ForEach(c => c.Addresses = adds.Where(a => a.CustomerId == c.CustomerId));
                adds = null;
                return custs.AsEnumerable<CounterpointCustomerPOCO>();
            }
        }


        public NetsuiteRecordWrapper ToCustomer(CounterpointCustomerPOCO input)
        {
            var externalId = "CS-" + input.CustomerId;
            if (!input.GsvVendNo.IsNullOrEmpty() && input.VendorUpserted == 0)
            {
                log.ErrorFormat("Customer {0} has GSVVendor {1}, vendor not upserted", externalId, input.GsvVendNo ?? "");
                return null;
            }
            LoadTypeInfo();


            RecordRef catRef = null;
            if (!input.Category.IsNullOrEmpty() && _customerCategories.Keys.Contains(input.Category))
                catRef = _helper.RecordToRef(_customerCategories[input.Category]);
            var finalFirstLast = input.FullName.ReconcileFirstLastNAMFromCP(input.FirstName.ValueOrDefault(""), input.LastName.ValueOrDefault(""));

            var cust = new Customer()
            {
                customForm = new RecordRef()
                {
                    //Must be set to allow the setting of the GSV fields etc.
                    internalId = Configuration.Get("FormCustomer")
                },
                externalId = externalId,
                isPerson = true,
                isPersonSpecified = true,
                firstName = (finalFirstLast.first.IsNullOrEmpty()) ? "_" : finalFirstLast.first,
                lastName = (finalFirstLast.last.IsNullOrEmpty()) ? "_" : finalFirstLast.last,
                companyName = string.Empty,
                category = catRef,
                entityStatus = (input.Status.Contains("Lost Customer")) ? _customerLostStatus : _defaultStatus,
                email = input.Email.GetEmailForNetsuite(),
                altEmail = input.AltEmail.GetEmailForNetsuite(),
                phone = input.Phone.GetPhoneNumberOrDefault(""),
                altPhone = input.AltEmail.GetPhoneNumberOrDefault(""),
                mobilePhone = input.MobilePhone.GetPhoneNumberOrDefault(null),
                addressbookList = GetAddressesForCustomer(input.Addresses)
            };

            List<CustomFieldRef> custFields = new List<CustomFieldRef>();
            input.DriversLicenseNo.CustomFieldStringValue("custentity_dl_no", custFields);
            input.DriversLicenseNo.CustomFieldStringValue("custentity_dl_no", custFields);
            input.DriversLicenseGender.CustomFieldStringValue("custentity_dl_gender", custFields);
            input.DriversLicenseEyeColor.CustomFieldStringValue("custentity_dl_eyecolor", custFields);
            input.DriversLicenseHeightVarchar.CustomFieldStringValue("custentity_dl_height", custFields);
            input.MARKETING_MAILOUTS_OPT_OUT_DAT.GetValueOrDefault(DateTime.Now).ToString("yyyy-MM-dd").CustomFieldDateValue("custentity_cp_marketing_optoutdate", custFields, log);
            ((input.MARKETING_MAILOUTS_OPT_OUT ?? "N") == "Y").CustomFieldCheckboxValue("custentity_cp_marketing_optout", custFields);

            //Link to Vendor Entity:
            if (!input.GsvVendNo.IsNullOrEmpty())
                custFields.Add(new SelectCustomFieldRef() { scriptId = "custentity_entitylink", value = new ListOrRecordRef() { externalId = "VN-" + input.GsvVendNo, typeId = "-3" } });
            //String Vend No:
            if (!input.GsvVendNo.IsNullOrEmpty())
                input.GsvVendNo.CustomFieldStringValue("custentity_gsv_vendno", custFields);

            if (input.TradeAppPhone.GetPhoneNumberOrDefault("null") != null)
                input.TradeAppPhone.GetPhoneNumberOrDefault("null").CustomFieldStringValue("custentity_tradeapp_phone", custFields);
            ((input.GsvFreeShipping ?? "N") == "Y").CustomFieldCheckboxValue("custentity_freeshipping", custFields);
            input.GsvShippingLabelPrice.CustomFieldCurrencyValue("custentity_shippinglabelprice", custFields);
            input.GsvMinimumProductBidPrice.CustomFieldCurrencyValue("custentity_minprodbidprice", custFields);

            if (!input.GsvSiteGUID.IsNullOrEmpty() && _validGSVSites.Contains(input.GsvSiteGUID))
                input.GsvSiteGUID.CustomRecordFieldLookupValue(_helper, "customrecord_gsv_site", "custentity_gsvsite_ref", custFields);

            input.GsvTradeAppFacilityName.CustomFieldStringValue("custentity_tradeapp_facilityname", custFields);

            if (input.DriversLicenseDateOfBirth != null)
                input.DriversLicenseDateOfBirth.GetValueOrDefault(DateTime.Now).ToString("yyyy-MM-dd").CustomFieldDateValue("custentity_dl_dob", custFields, log);
            input.DriversLicenseWeight.CustomFieldFloatValue("custentity_dl_weight", custFields);
            input.DriversLicenseRace.CustomFieldStringValue("custentity_dl_race", custFields);
            input.DriversLicenseHeightVarchar.CustomFieldIntValue("custentity_dl_height_measure", custFields);

            if (!input.GsvVendNo.IsNullOrEmpty())
            {
                custFields.Add(
                    new SelectCustomFieldRef() {
                        scriptId = "custentity_entitylink", value = new ListOrRecordRef() { externalId = ("VN-" + input.GsvVendNo), typeId = "-3" }
                    }
                );
            }
            if (externalId.StartsWith("W-"))
            {
                custFields.Add( new LongCustomFieldRef() { scriptId = "custentity_aspdnsf_id", value = int.Parse(externalId.Replace("W-", "")) });
            }

            cust.customFieldList = custFields.ToArray();
            return new NetsuiteRecordWrapper() { NetsuiteRecord = cust, ExternalId = externalId, LocalId = input.CustomerId, LocalHash = input.CurrentHash, RecordType = "customer" };
        }

        private CustomerAddressbookList GetAddressesForCustomer(IEnumerable<CounterpointCustomerAddressPOCO> addresses)
        {
            List<CustomerAddressbook> finalAddresses = addresses.Select(a =>
               new CustomerAddressbook()
               {
                   defaultBilling = a.IsDefault,
                   defaultBillingSpecified = a.IsDefault,
                   addressbookAddress = new Address()
                   {
                       addressee = a.Addressee,
                       addr1 = a.Address1,
                       addr2 = a.Address2,
                       addr3 = a.Address3,
                       city = a.City,
                       state = a.State,
                       zip = a.Zip,
                       customFieldList = new CustomFieldRef[] {
                         new StringCustomFieldRef() { scriptId = "custrecord_address_country", value = a.Country??"" }
                        }
                   }
               }
            ).ToList();

            return new CustomerAddressbookList()
            {
                replaceAll = true,
                addressbook = finalAddresses.ToArray()
            };
        }

    }
}
